﻿Public Class FormKelas2
        Private conn As New MySqlConnection
        Dim connString = "Database=kereta_api; Data source=localhost; user id=root; password=;"

        Private Sub Bersihkan()
        txt_kelas.Text = ""
        txt_tiket.Text = ""
        txt_gerbong.Text = ""
        txt_jadwal.Text = ""
        cmb_kelas.Text = ""
        txt_harga.Text = ""
        Txt_Tuju.Text = ""
        Txt_total.Text = ""
        txt_kelas.Focus()
        Btn_tambah.Enabled = True
        Btn_ubah.Enabled = True
        Btn_hapus.Enabled = True
        End Sub


    Private Sub Btn_Cari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Cari.Click
        CariByIDK()
    End Sub

    Private Sub Btn_bersih_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_bersih.Click
        Bersihkan()
    End Sub

        Private Sub CariByIDK()
            Dim connString = "Database=kereta_api; Data source=localhost; user id=root; password=;"
            conn = New MySqlConnection(connString)
            conn.Open()

            Dim dr As MySqlDataReader = Nothing
            Dim cmdSelect As MySqlCommand = conn.CreateCommand

            Try
                If txt_kelas.Text <> "" Then
                    Dim sqlString As String = "select * from t_kelas where id_kelas=@id_kelas;"
                cmdSelect.Parameters.Add("@id_kelas", MySqlDbType.String, 20).Value = txt_kelas.Text
                    cmdSelect.CommandText = sqlString
                    dr = cmdSelect.ExecuteReader

                    If dr.Read() Then
                    txt_kelas.Text = dr("id_kelas").ToString
                    txt_tiket.Text = dr("id_tiket").ToString
                    txt_gerbong.Text = dr("id_gerbong").ToString
                    txt_jadwal.Text = dr("id_jadwal").ToString
                    cmb_kelas.Text = dr("jenis_kelas").ToString
                    txt_harga.Text = dr("harga").ToString
                    Txt_Tuju.Text = dr("tujuan").ToString
                    Txt_total.Text = dr("total_harga").ToString
                    End If
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Terjadi kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Finally
                cmdSelect.Dispose()
                'conn.Close()
                conn = Nothing
            End Try
        End Sub

        Private Sub TampilkanDaftar()
        Dim da As New MySqlDataAdapter()
        Dim ds As New DataSet()
        Dim connString = "Database=kereta_api; Data source=localhost; user id=root;password;"
        conn = New MySqlConnection(connString)
        Dim cmdSelect As MySqlCommand = conn.CreateCommand()
        Dim sqlString As String
        Try
            If txt_kelas.Text <> "" Then
                sqlString = "Select * from vw_data_tiket where id_kelas=@id_kelas;"
                cmdSelect.Parameters.Add("@kode", MySqlDbType.String, 10).Value = txt_kelas.Text
            Else
                sqlString = "Select * from vw_data_tiket;"
            End If
            cmdSelect.CommandText = sqlString
            da.SelectCommand = cmdSelect
            da.Fill(ds, "vw_data_tiket")
            DataGridView1.DataSource = ds
            DataGridView1.DataMember = "vw_data_tiket"
            DataGridView1.ReadOnly = True

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Terjadi Kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            cmdSelect.Dispose()
            conn.Close()
            conn = Nothing
        End Try
    End Sub




















    Private Sub Btn_Keluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Keluar.Click
        Me.Close()
    End Sub


    Private Sub Btn_ubah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_ubah.Click
        Dim connString = "Database=kereta_api;Data source=localhost;user id=root;password=;"
        conn = New MySqlConnection(connString)
        conn.Open()
        Dim cmdUpdate As MySqlCommand = conn.CreateCommand
        If txt_kelas.Text <> "" Then
            Try
                Dim sqlString As String = "update t_kelas set id_kelas=@id_kelas,id_tiket=@id_tiket,id_gerbong=@id_gerbong,id_jadwal=@id_jadwal,jenis_kelas=@jenis_kelas,harga=@harga,tujuan=@tujuan,total_harga=@total_harga where id_kelas=@id_kelas"
                cmdUpdate.CommandText = sqlString
                cmdUpdate.Connection = conn
                cmdUpdate.Parameters.Add("@id_kelas", MySqlDbType.String, 20).Value = txt_kelas.Text
                cmdUpdate.Parameters.Add("@id_tiket", MySqlDbType.String, 20).Value = txt_tiket.Text
                cmdUpdate.Parameters.Add("@id_gerbong", MySqlDbType.String, 20).Value = txt_gerbong.Text
                cmdUpdate.Parameters.Add("@id_jadwal", MySqlDbType.String, 20).Value = txt_jadwal.Text
                cmdUpdate.Parameters.Add("@jenis_kelas", MySqlDbType.String, 20).Value = cmb_kelas.Text
                cmdUpdate.Parameters.Add("@harga", MySqlDbType.Int16, 20).Value = txt_harga.Text
                cmdUpdate.Parameters.Add("@tujuan", MySqlDbType.String, 20).Value = Txt_Tuju.Text
                cmdUpdate.Parameters.Add("@total_harga", MySqlDbType.String, 20).Value = Txt_total.Text
                cmdUpdate.ExecuteNonQuery()
                MessageBox.Show("Data Berhasil diubah!", "Project Kereta Api", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Bersihkan()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Terjadi Kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Finally
                cmdUpdate.Dispose()
                'conn.Close()
                conn = Nothing
            End Try
        End If
        TampilkanDaftar()
    End Sub


















    Private Sub Btn_hapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_hapus.Click
        Dim connString = "Database=kereta_api; Data source=localhost; user id=root; password=;"
        conn = New MySqlConnection(connString)
        conn.Open()

        Dim cmdDelete As MySqlCommand = conn.CreateCommand

        If txt_kelas.Text <> "" Then
            If MessageBox.Show("Anda yakin data kelas dengan Id_kelas " & txt_kelas.Text & " akan dihapus ?", "Peringatan!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                Try
                    Dim sqlString As String = "delete from t_kelas where id_kelas=@id_kelas;"
                    cmdDelete.CommandText = sqlString
                    cmdDelete.Connection = conn
                    cmdDelete.Parameters.Add("@id_kelas", MySqlDbType.String, 10).Value = txt_kelas.Text
                    cmdDelete.ExecuteNonQuery()
                    MessageBox.Show("Data berhasil dihapus", "My Project", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Bersihkan()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Terjadi kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Finally
                    cmdDelete.Dispose()
                    'conn.Close()
                    conn = Nothing
                End Try
            End If
            TampilkanDaftar()
        End If
    End Sub

    Private Sub Btn_tambah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_tambah.Click
        Dim connString = "Database=kereta_api; Data source=localhost; user id=root; password=;"
        conn = New MySqlConnection(connString)
        conn.Open()

        Dim cmdInsert As MySqlCommand = conn.CreateCommand

        If txt_kelas.Text <> "" Then
            Try
                Dim sqlString As String = "insert into t_kelas (id_kelas, id_tiket,id_gerbong, id_jadwal, jenis_kelas,harga,tujuan, total_harga) values (@id_kelas, @id_tiket,@id_gerbong, @id_jadwal, @jenis_kelas,@harga,@tujuan, @total_harga);"
                cmdInsert.CommandText = sqlString
                cmdInsert.Connection = conn
                cmdInsert.Parameters.Add("@id_kelas", MySqlDbType.String, 20).Value = txt_kelas.Text
                cmdInsert.Parameters.Add("@id_tiket", MySqlDbType.String, 20).Value = txt_tiket.Text
                cmdInsert.Parameters.Add("@id_gerbong", MySqlDbType.String, 20).Value = txt_gerbong.Text
                cmdInsert.Parameters.Add("@id_jadwal", MySqlDbType.String, 20).Value = txt_jadwal.Text
                cmdInsert.Parameters.Add("@jenis_kelas", MySqlDbType.String, 25).Value = cmb_kelas.Text
                cmdInsert.Parameters.Add("@harga", MySqlDbType.String, 25).Value = txt_harga.Text
                cmdInsert.Parameters.Add("@tujuan", MySqlDbType.String, 20).Value = Txt_Tuju.Text
                cmdInsert.Parameters.Add("@total_harga", MySqlDbType.String, 20).Value = Txt_total.Text
                cmdInsert.ExecuteNonQuery()
                MessageBox.Show("Data berhasil ditambahkan", "My Project", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Bersihkan()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Terjadi kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Finally
                cmdInsert.Dispose()
                'conn.Close()
                conn = Nothing
            End Try
        End If
        TampilkanDaftar()
    End Sub

    Private Sub cmb_kelas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmb_kelas.SelectedIndexChanged
        If cmb_kelas.Text = "Eksekutif" Then
            txt_harga.Text = "10000"
        ElseIf cmb_kelas.Text = "Bussiness" Then
            txt_harga.Text = "7500"
        Else
            txt_harga.Text = "2500"
        End If
    End Sub

    Private Sub FormKelas2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        TampilkanDaftar()
    End Sub

    Private Sub txt_tiket_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_tiket.TextChanged
        Dim connString = "Database=kereta_api;data source=localhost;user id=root;password=;"
        conn = New MySqlConnection(connString)
        conn.Open()

        Dim dr As MySqlDataReader = Nothing
        Dim cmdSelect As MySqlCommand = conn.CreateCommand()
        Try
            If txt_tiket.Text <> "" Then
                Dim sqlString As String = "Select * from t_tiket where id_tiket=@id_tiket;"
                cmdSelect.Parameters.Add("@id_tiket", MySqlDbType.String, 20).Value = txt_tiket.Text
                cmdSelect.CommandText = sqlString
                dr = cmdSelect.ExecuteReader

                If dr.Read() Then
                    txt_tiket.Text = dr("id_tiket").ToString
                    Txt_jml.Text = dr("jumlah_tiket").ToString
                Else

                    Txt_jml.Text = ""
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Terjadi Kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            cmdSelect.Dispose()
            'conn.Close()
            conn = Nothing
        End Try
    End Sub

    Private Sub txt_gerbong_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_gerbong.TextChanged
        Dim connString = "Database=kereta_api;data source=localhost;user id=root;password=;"
        conn = New MySqlConnection(connString)
        conn.Open()

        Dim dr As MySqlDataReader = Nothing
        Dim cmdSelect As MySqlCommand = conn.CreateCommand()
        Try
            If txt_gerbong.Text <> "" Then
                Dim sqlString As String = "Select * from t_gerbong where id_gerbong=@id_gerbong;"
                cmdSelect.Parameters.Add("@id_gerbong", MySqlDbType.String, 20).Value = txt_gerbong.Text
                cmdSelect.CommandText = sqlString
                dr = cmdSelect.ExecuteReader

                If dr.Read() Then
                    txt_gerbong.Text = dr("id_gerbong").ToString
                    Txt_nama.Text = dr("nama_gerbong").ToString
                Else

                    Txt_nama.Text = ""
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Terjadi Kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            cmdSelect.Dispose()
            'conn.Close()
            conn = Nothing
        End Try
    End Sub

    Private Sub txt_jadwal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_jadwal.TextChanged
        Dim connString = "Database=kereta_api;data source=localhost;user id=root;password=;"
        conn = New MySqlConnection(connString)
        conn.Open()

        Dim dr As MySqlDataReader = Nothing
        Dim cmdSelect As MySqlCommand = conn.CreateCommand()
        Try
            If txt_jadwal.Text <> "" Then
                Dim sqlString As String = "Select * from t_jadwal where id_jadwal=@id_jadwal;"
                cmdSelect.Parameters.Add("@id_jadwal", MySqlDbType.String, 20).Value = txt_jadwal.Text
                cmdSelect.CommandText = sqlString
                dr = cmdSelect.ExecuteReader

                If dr.Read() Then
                    txt_jadwal.Text = dr("id_jadwal").ToString
                    Txt_waktu.Text = dr("waktu_keberangkatan").ToString
                Else

                    Txt_waktu.Text = ""
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Terjadi Kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            cmdSelect.Dispose()
            'conn.Close()
            conn = Nothing
        End Try
    End Sub

    Private Sub Btn_Cetak_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Cetak.Click
        Dim connString = "Database=kereta_api; Data source=localhost; user id=root; password=;"
        Dim da As New MySqlDataAdapter
        Dim ds As New DataSet
        Dim sqlString As String
        conn = New MySqlConnection(connString)
        Dim cmd As MySqlCommand = conn.CreateCommand
        conn.Open()

        Try
            ds.Clear()
            sqlString = "select * from vw_rpt_data_pembelian;"

            cmd = New MySqlCommand(sqlString, conn)
            da.SelectCommand = cmd
            da.Fill(ds, "vw_rpt_data_pembelian")

            Dim laporan As New rptDataPembelian
            laporan.SetDataSource(ds)
            frmViewLaporan.CrystalReportViewer1.ReportSource = laporan
            frmViewLaporan.ShowDialog()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Terjadi Kegagalan !", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            conn.Close()
            conn = Nothing
        End Try
    End Sub

    Private Sub txt_harga_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga.TextChanged
        Txt_total.Text = Val(txt_harga.Text) * Val(Txt_jml.Text)
    End Sub
End Class