﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class formjadwal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(formjadwal))
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_id = New System.Windows.Forms.TextBox()
        Me.LABEL_7 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.Btn_Cari = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Btn_Keluar = New System.Windows.Forms.Button()
        Me.Btn_Hapus = New System.Windows.Forms.Button()
        Me.Btn_tambah = New System.Windows.Forms.Button()
        Me.Btn_Bersih = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(34, 128)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(176, 20)
        Me.Label7.TabIndex = 61
        Me.Label7.Text = "WAKTU KEBERANGKATAN"
        '
        'txt_id
        '
        Me.txt_id.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_id.Location = New System.Drawing.Point(216, 94)
        Me.txt_id.Name = "txt_id"
        Me.txt_id.Size = New System.Drawing.Size(154, 26)
        Me.txt_id.TabIndex = 60
        '
        'LABEL_7
        '
        Me.LABEL_7.AutoSize = True
        Me.LABEL_7.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LABEL_7.Location = New System.Drawing.Point(111, 97)
        Me.LABEL_7.Name = "LABEL_7"
        Me.LABEL_7.Size = New System.Drawing.Size(79, 20)
        Me.LABEL_7.TabIndex = 59
        Me.LABEL_7.Text = "ID JADWAL"
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(96, 160)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(305, 174)
        Me.DataGridView1.TabIndex = 69
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.DateTimePicker1.Location = New System.Drawing.Point(216, 126)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(154, 26)
        Me.DateTimePicker1.TabIndex = 70
        '
        'Btn_Cari
        '
        Me.Btn_Cari.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Cari.Image = CType(resources.GetObject("Btn_Cari.Image"), System.Drawing.Image)
        Me.Btn_Cari.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_Cari.Location = New System.Drawing.Point(376, 94)
        Me.Btn_Cari.Name = "Btn_Cari"
        Me.Btn_Cari.Size = New System.Drawing.Size(75, 36)
        Me.Btn_Cari.TabIndex = 75
        Me.Btn_Cari.Text = "CARI"
        Me.Btn_Cari.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Cari.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.SystemColors.HighlightText
        Me.Label4.Font = New System.Drawing.Font("Arial Narrow", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(129, 38)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(460, 37)
        Me.Label4.TabIndex = 81
        Me.Label4.Text = "DATA JADWAL KEBERANGKATAN"
        '
        'Btn_Keluar
        '
        Me.Btn_Keluar.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Keluar.Image = CType(resources.GetObject("Btn_Keluar.Image"), System.Drawing.Image)
        Me.Btn_Keluar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_Keluar.Location = New System.Drawing.Point(16, 146)
        Me.Btn_Keluar.Name = "Btn_Keluar"
        Me.Btn_Keluar.Size = New System.Drawing.Size(101, 39)
        Me.Btn_Keluar.TabIndex = 95
        Me.Btn_Keluar.Text = "Keluar"
        Me.Btn_Keluar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Keluar.UseVisualStyleBackColor = True
        '
        'Btn_Hapus
        '
        Me.Btn_Hapus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Btn_Hapus.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Hapus.Image = CType(resources.GetObject("Btn_Hapus.Image"), System.Drawing.Image)
        Me.Btn_Hapus.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Hapus.Location = New System.Drawing.Point(16, 63)
        Me.Btn_Hapus.Name = "Btn_Hapus"
        Me.Btn_Hapus.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Btn_Hapus.Size = New System.Drawing.Size(101, 35)
        Me.Btn_Hapus.TabIndex = 94
        Me.Btn_Hapus.Text = "Hapus"
        Me.Btn_Hapus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_Hapus.UseVisualStyleBackColor = True
        '
        'Btn_tambah
        '
        Me.Btn_tambah.BackColor = System.Drawing.SystemColors.Control
        Me.Btn_tambah.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_tambah.Image = CType(resources.GetObject("Btn_tambah.Image"), System.Drawing.Image)
        Me.Btn_tambah.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_tambah.Location = New System.Drawing.Point(16, 16)
        Me.Btn_tambah.Name = "Btn_tambah"
        Me.Btn_tambah.Size = New System.Drawing.Size(101, 39)
        Me.Btn_tambah.TabIndex = 92
        Me.Btn_tambah.Text = "Tambah"
        Me.Btn_tambah.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_tambah.UseVisualStyleBackColor = False
        '
        'Btn_Bersih
        '
        Me.Btn_Bersih.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Bersih.Image = CType(resources.GetObject("Btn_Bersih.Image"), System.Drawing.Image)
        Me.Btn_Bersih.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_Bersih.Location = New System.Drawing.Point(16, 104)
        Me.Btn_Bersih.Name = "Btn_Bersih"
        Me.Btn_Bersih.Size = New System.Drawing.Size(101, 36)
        Me.Btn_Bersih.TabIndex = 91
        Me.Btn_Bersih.Text = "Bersih"
        Me.Btn_Bersih.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Bersih.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Btn_tambah)
        Me.GroupBox1.Controls.Add(Me.Btn_Keluar)
        Me.GroupBox1.Controls.Add(Me.Btn_Bersih)
        Me.GroupBox1.Controls.Add(Me.Btn_Hapus)
        Me.GroupBox1.Location = New System.Drawing.Point(472, 94)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(130, 210)
        Me.GroupBox1.TabIndex = 96
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "OPERASI"
        '
        'formjadwal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(794, 397)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Btn_Cari)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txt_id)
        Me.Controls.Add(Me.LABEL_7)
        Me.Name = "formjadwal"
        Me.Text = "formjadwal"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txt_id As System.Windows.Forms.TextBox
    Friend WithEvents LABEL_7 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Btn_Cari As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Btn_Keluar As System.Windows.Forms.Button
    Friend WithEvents Btn_Hapus As System.Windows.Forms.Button
    Friend WithEvents Btn_tambah As System.Windows.Forms.Button
    Friend WithEvents Btn_Bersih As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
End Class
