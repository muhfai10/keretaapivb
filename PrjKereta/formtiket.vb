﻿Public Class formtiket
    Private conn As New MySqlConnection
    Dim connString = "Database=kereta_api;data source=localhost;user id=root;password=;"
    Private Sub formtiket_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        tampilkandaftar()
    End Sub

    Private Sub Btn_tambah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_tambah.Click
        Dim connString = "Database=kereta_api; Data source=localhost; user id=root; password=;"
        conn = New MySqlConnection(connString)
        conn.Open()

        Dim cmdInsert As MySqlCommand = conn.CreateCommand

        If txt_idtiket.Text <> "" Then
            Try
                Dim SqlString As String = "insert into t_tiket (id_tiket,id_pelanggan,jumlah_tiket,tanggal_pembelian) values (@id_tiket,@id_pelanggan,@jumlah_tiket,@tanggal_pembelian);"
                cmdInsert.CommandText = SqlString
                cmdInsert.Connection = conn
                cmdInsert.Parameters.Add("@id_tiket", MySqlDbType.String, 20).Value = txt_idtiket.Text
                cmdInsert.Parameters.Add("@id_pelanggan", MySqlDbType.String, 20).Value = txt_idpel.Text
                cmdInsert.Parameters.Add("@jumlah_tiket", MySqlDbType.Int16, 3).Value = txt_jml.Text
                cmdInsert.Parameters.Add("@tanggal_pembelian", MySqlDbType.Date).Value = DateTimePicker1.Text
                cmdInsert.ExecuteNonQuery()
                MessageBox.Show("data Berhasil ditambahkan", "My Project", MessageBoxButtons.OK, MessageBoxIcon.Information)
                bersihkan()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Terjadi Kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Finally
                cmdInsert.Dispose()
                'conn.Close()
                conn = Nothing
            End Try
        End If
        tampilkandaftar()
    End Sub

    Private Sub Btn_Ubah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Ubah.Click
        conn = New MySqlConnection(connString)
        conn.Open()

        Dim cmdInsert As MySqlCommand = conn.CreateCommand

        If txt_idtiket.Text <> "" Then
            Try
                Dim SqlString As String = " update t_tiket set id_pelanggan=@id_pelanggan,jumlah_tiket=@jumlah_tiket,tanggal_pembelian=@tanggal_pembelian  where id_tiket=@id_tiket;"
                cmdInsert.CommandText = SqlString
                cmdInsert.Connection = conn
                cmdInsert.Parameters.Add("@id_tiket", MySqlDbType.String, 20).Value = txt_idtiket.Text
                cmdInsert.Parameters.Add("@id_pelanggan", MySqlDbType.String, 20).Value = txt_idpel.Text
                cmdInsert.Parameters.Add("@jumlah_tiket", MySqlDbType.String, 20).Value = txt_jml.Text
                cmdInsert.Parameters.Add("@tanggal_pembelian", MySqlDbType.Date).Value = DateTimePicker1.Text
                cmdInsert.ExecuteNonQuery()
                MessageBox.Show("data Berhasil diubah", "My Project", MessageBoxButtons.OK, MessageBoxIcon.Information)
                bersihkan()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Terjadi Kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Finally
                cmdInsert.Dispose()
                'conn.Close()
                conn = Nothing
            End Try
        End If
        tampilkandaftar()
    End Sub

    Private Sub tampilkandaftar()
        Dim da As New MySqlDataAdapter()
        Dim ds As New DataSet()
        Dim connString = "Database=kereta_api; Data source=localhost; user id=root; password=;"
        conn = New MySqlConnection(connString)
        Dim cmdSelect As MySqlCommand = conn.CreateCommand()
        Dim sqlString As String
        Try
            If txt_idtiket.Text <> "" Then
                sqlString = "select * from t_tiket where id_tiket=@id_tiket;"
                cmdSelect.Parameters.Add("@kode", MySqlDbType.String, 20).Value = txt_idtiket.Text
            Else
                sqlString = "select * from t_tiket;"
            End If

            cmdSelect.CommandText = sqlString

            da.SelectCommand = cmdSelect
            da.Fill(ds, "t_tiket")
            DataGridView1.DataSource = ds
            DataGridView1.DataMember = "t_tiket"
            DataGridView1.ReadOnly = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Terjadi kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            cmdSelect.Dispose()
            conn.Close()
            conn = Nothing
        End Try
    End Sub

    Private Sub Btn_Bersih_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Bersih.Click
        bersihkan()
    End Sub
    Sub bersihkan()
        txt_idtiket.Text = ""
        txt_idpel.Text = ""
        txt_jml.Text = ""
        txt_nama.Text = ""
        Btn_tambah.Enabled = True
    End Sub

    Private Sub Btn_Cari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Cari.Click
        conn = New MySqlConnection(connString)
        conn.Open()

        Dim dr As MySqlDataReader = Nothing
        Dim cmdSelect As MySqlCommand = conn.CreateCommand()
        Try
            If txt_idtiket.Text <> "" Then
                Dim sqlString As String = "Select * from t_tiket where id_tiket=@id_tiket;"
                cmdSelect.Parameters.Add("@id_tiket", MySqlDbType.String, 20).Value = txt_idtiket.Text
                cmdSelect.CommandText = sqlString
                dr = cmdSelect.ExecuteReader

                If dr.Read() Then
                    txt_idtiket.Text = dr("id_tiket").ToString
                    txt_idpel.Text = dr("id_pelanggan").ToString
                    DateTimePicker1.Text = dr("tanggal_pembelian").ToString
                    txt_jml.Text = dr("jumlah_tiket").ToString

                    Btn_tambah.Enabled = False
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Terjadi Kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            cmdSelect.Dispose()
            'conn.Close()
            conn = Nothing
        End Try
    End Sub

    Private Sub Btn_keluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_keluar.Click
        Me.Close()
    End Sub

    Private Sub Btn_Hapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Hapus.Click
        conn = New MySqlConnection(connString)
        conn.Open()

        Dim cmdDelete As MySqlCommand = conn.CreateCommand
        If txt_idtiket.Text <> "" Then
            If MessageBox.Show("Anda yakin data tiket dengan id tiket " & txt_idtiket.Text & " akan dihapus?", "Peringatan!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                Try
                    Dim sqlString As String = "delete from t_tiket where id_tiket=@id_tiket;"
                    cmdDelete.CommandText = sqlString
                    cmdDelete.Connection = conn
                    cmdDelete.Parameters.Add("@id_tiket", MySqlDbType.String, 20).Value = txt_idtiket.Text
                    cmdDelete.ExecuteNonQuery()
                    MessageBox.Show("data Berhasil dihapus", "My Project", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    bersihkan()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Terjadi Kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Finally
                    cmdDelete.Dispose()
                    'conn.Close()
                    conn = Nothing
                End Try
            End If
        End If
        tampilkandaftar()
    End Sub

    Private Sub txt_idpel_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_idpel.TextChanged
        conn = New MySqlConnection(connString)
        conn.Open()

        Dim dr As MySqlDataReader = Nothing
        Dim cmdSelect As MySqlCommand = conn.CreateCommand()
        Try
            If txt_idpel.Text <> "" Then
                Dim sqlString As String = "Select * from t_pelanggan where id_pelanggan=@id_pelanggan;"
                cmdSelect.Parameters.Add("@id_pelanggan", MySqlDbType.String, 20).Value = txt_idpel.Text
                cmdSelect.CommandText = sqlString
                dr = cmdSelect.ExecuteReader

                If dr.Read() Then
                    txt_idpel.Text = dr("id_pelanggan").ToString
                    txt_nama.Text = dr("nama").ToString
                Else

                    txt_nama.Text = ""
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Terjadi Kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            cmdSelect.Dispose()
            'conn.Close()
            conn = Nothing
        End Try
    End Sub


End Class