﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class formtiket
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(formtiket))
        Me.txt_idtiket = New System.Windows.Forms.TextBox()
        Me.LABEL9 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_jml = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.Btn_keluar = New System.Windows.Forms.Button()
        Me.Btn_Hapus = New System.Windows.Forms.Button()
        Me.Btn_Ubah = New System.Windows.Forms.Button()
        Me.Btn_tambah = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Btn_Cari = New System.Windows.Forms.Button()
        Me.Btn_Bersih = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_idpel = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_nama = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.OPERASI = New System.Windows.Forms.GroupBox()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.OPERASI.SuspendLayout()
        Me.SuspendLayout()
        '
        'txt_idtiket
        '
        Me.txt_idtiket.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_idtiket.Location = New System.Drawing.Point(233, 81)
        Me.txt_idtiket.Name = "txt_idtiket"
        Me.txt_idtiket.Size = New System.Drawing.Size(200, 26)
        Me.txt_idtiket.TabIndex = 56
        '
        'LABEL9
        '
        Me.LABEL9.AutoSize = True
        Me.LABEL9.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LABEL9.Location = New System.Drawing.Point(46, 87)
        Me.LABEL9.Name = "LABEL9"
        Me.LABEL9.Size = New System.Drawing.Size(67, 20)
        Me.LABEL9.TabIndex = 55
        Me.LABEL9.Text = "ID_TIKET"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(46, 211)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(153, 20)
        Me.Label7.TabIndex = 59
        Me.Label7.Text = "TANGGAL PEMBELIAN"
        '
        'txt_jml
        '
        Me.txt_jml.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_jml.Location = New System.Drawing.Point(233, 180)
        Me.txt_jml.Name = "txt_jml"
        Me.txt_jml.Size = New System.Drawing.Size(200, 26)
        Me.txt_jml.TabIndex = 58
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(46, 180)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(107, 20)
        Me.Label1.TabIndex = 57
        Me.Label1.Text = "JUMLAH_TIKET"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(233, 211)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(200, 26)
        Me.DateTimePicker1.TabIndex = 61
        '
        'Btn_keluar
        '
        Me.Btn_keluar.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_keluar.Image = CType(resources.GetObject("Btn_keluar.Image"), System.Drawing.Image)
        Me.Btn_keluar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_keluar.Location = New System.Drawing.Point(38, 183)
        Me.Btn_keluar.Name = "Btn_keluar"
        Me.Btn_keluar.Size = New System.Drawing.Size(116, 39)
        Me.Btn_keluar.TabIndex = 72
        Me.Btn_keluar.Text = "KELUAR"
        Me.Btn_keluar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_keluar.UseVisualStyleBackColor = True
        '
        'Btn_Hapus
        '
        Me.Btn_Hapus.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Hapus.Image = CType(resources.GetObject("Btn_Hapus.Image"), System.Drawing.Image)
        Me.Btn_Hapus.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_Hapus.Location = New System.Drawing.Point(38, 148)
        Me.Btn_Hapus.Name = "Btn_Hapus"
        Me.Btn_Hapus.Size = New System.Drawing.Size(116, 31)
        Me.Btn_Hapus.TabIndex = 71
        Me.Btn_Hapus.Text = "HAPUS"
        Me.Btn_Hapus.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Hapus.UseVisualStyleBackColor = True
        '
        'Btn_Ubah
        '
        Me.Btn_Ubah.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Ubah.Image = CType(resources.GetObject("Btn_Ubah.Image"), System.Drawing.Image)
        Me.Btn_Ubah.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_Ubah.Location = New System.Drawing.Point(39, 64)
        Me.Btn_Ubah.Name = "Btn_Ubah"
        Me.Btn_Ubah.Size = New System.Drawing.Size(115, 33)
        Me.Btn_Ubah.TabIndex = 70
        Me.Btn_Ubah.Text = "UBAH"
        Me.Btn_Ubah.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Ubah.UseVisualStyleBackColor = True
        '
        'Btn_tambah
        '
        Me.Btn_tambah.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_tambah.Image = CType(resources.GetObject("Btn_tambah.Image"), System.Drawing.Image)
        Me.Btn_tambah.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_tambah.Location = New System.Drawing.Point(38, 19)
        Me.Btn_tambah.Name = "Btn_tambah"
        Me.Btn_tambah.Size = New System.Drawing.Size(116, 39)
        Me.Btn_tambah.TabIndex = 69
        Me.Btn_tambah.Text = "TAMBAH"
        Me.Btn_tambah.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_tambah.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(50, 329)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(444, 142)
        Me.DataGridView1.TabIndex = 73
        '
        'Btn_Cari
        '
        Me.Btn_Cari.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Cari.Image = CType(resources.GetObject("Btn_Cari.Image"), System.Drawing.Image)
        Me.Btn_Cari.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_Cari.Location = New System.Drawing.Point(447, 81)
        Me.Btn_Cari.Name = "Btn_Cari"
        Me.Btn_Cari.Size = New System.Drawing.Size(75, 33)
        Me.Btn_Cari.TabIndex = 74
        Me.Btn_Cari.Text = "CARI"
        Me.Btn_Cari.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Cari.UseVisualStyleBackColor = True
        '
        'Btn_Bersih
        '
        Me.Btn_Bersih.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Bersih.Image = CType(resources.GetObject("Btn_Bersih.Image"), System.Drawing.Image)
        Me.Btn_Bersih.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_Bersih.Location = New System.Drawing.Point(38, 103)
        Me.Btn_Bersih.Name = "Btn_Bersih"
        Me.Btn_Bersih.Size = New System.Drawing.Size(116, 35)
        Me.Btn_Bersih.TabIndex = 75
        Me.Btn_Bersih.Text = "BERSIH"
        Me.Btn_Bersih.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Bersih.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(46, 119)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(111, 20)
        Me.Label2.TabIndex = 76
        Me.Label2.Text = "ID_PELANGGAN"
        '
        'txt_idpel
        '
        Me.txt_idpel.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_idpel.Location = New System.Drawing.Point(233, 113)
        Me.txt_idpel.Name = "txt_idpel"
        Me.txt_idpel.Size = New System.Drawing.Size(200, 26)
        Me.txt_idpel.TabIndex = 77
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(46, 151)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(133, 20)
        Me.Label3.TabIndex = 78
        Me.Label3.Text = "NAMA PELANGGAN"
        '
        'txt_nama
        '
        Me.txt_nama.Enabled = False
        Me.txt_nama.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_nama.Location = New System.Drawing.Point(233, 145)
        Me.txt_nama.Name = "txt_nama"
        Me.txt_nama.Size = New System.Drawing.Size(200, 26)
        Me.txt_nama.TabIndex = 79
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial Narrow", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(265, 27)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(342, 37)
        Me.Label4.TabIndex = 80
        Me.Label4.Text = "DATA TIKET PENJUALAN"
        '
        'OPERASI
        '
        Me.OPERASI.BackColor = System.Drawing.Color.White
        Me.OPERASI.Controls.Add(Me.Btn_Hapus)
        Me.OPERASI.Controls.Add(Me.Btn_tambah)
        Me.OPERASI.Controls.Add(Me.Btn_Ubah)
        Me.OPERASI.Controls.Add(Me.Btn_keluar)
        Me.OPERASI.Controls.Add(Me.Btn_Bersih)
        Me.OPERASI.Location = New System.Drawing.Point(531, 81)
        Me.OPERASI.Name = "OPERASI"
        Me.OPERASI.Size = New System.Drawing.Size(181, 241)
        Me.OPERASI.TabIndex = 81
        Me.OPERASI.TabStop = False
        Me.OPERASI.Text = "OPERASI"
        '
        'formtiket
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(879, 546)
        Me.Controls.Add(Me.OPERASI)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txt_nama)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txt_idpel)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Btn_Cari)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txt_jml)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txt_idtiket)
        Me.Controls.Add(Me.LABEL9)
        Me.Name = "formtiket"
        Me.Text = "formtiket"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.OPERASI.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txt_idtiket As System.Windows.Forms.TextBox
    Friend WithEvents LABEL9 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txt_jml As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Btn_keluar As System.Windows.Forms.Button
    Friend WithEvents Btn_Hapus As System.Windows.Forms.Button
    Friend WithEvents Btn_Ubah As System.Windows.Forms.Button
    Friend WithEvents Btn_tambah As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Btn_Cari As System.Windows.Forms.Button
    Friend WithEvents Btn_Bersih As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txt_idpel As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txt_nama As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents OPERASI As System.Windows.Forms.GroupBox
End Class
