﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class formgerbong
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(formgerbong))
        Me.Btn_Cari = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Btn_Bersih = New System.Windows.Forms.Button()
        Me.Txt_Jumlah = New System.Windows.Forms.TextBox()
        Me.Txt_Posisi = New System.Windows.Forms.TextBox()
        Me.Txt_Id = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Cmb_nama = New System.Windows.Forms.ComboBox()
        Me.Btn_Keluar = New System.Windows.Forms.Button()
        Me.Btn_Hapus = New System.Windows.Forms.Button()
        Me.Btn_Ubah = New System.Windows.Forms.Button()
        Me.btn_Simpan = New System.Windows.Forms.Button()
        Me.txt_idp = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_namap = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Btn_Cari
        '
        Me.Btn_Cari.BackColor = System.Drawing.SystemColors.Control
        Me.Btn_Cari.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Cari.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Btn_Cari.Image = CType(resources.GetObject("Btn_Cari.Image"), System.Drawing.Image)
        Me.Btn_Cari.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.Btn_Cari.Location = New System.Drawing.Point(402, 71)
        Me.Btn_Cari.Name = "Btn_Cari"
        Me.Btn_Cari.Size = New System.Drawing.Size(75, 29)
        Me.Btn_Cari.TabIndex = 27
        Me.Btn_Cari.Text = "Cari"
        Me.Btn_Cari.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Cari.UseVisualStyleBackColor = False
        '
        'DataGridView1
        '
        Me.DataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonFace
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(114, 284)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(565, 169)
        Me.DataGridView1.TabIndex = 26
        '
        'Btn_Bersih
        '
        Me.Btn_Bersih.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Bersih.Image = CType(resources.GetObject("Btn_Bersih.Image"), System.Drawing.Image)
        Me.Btn_Bersih.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.Btn_Bersih.Location = New System.Drawing.Point(70, 120)
        Me.Btn_Bersih.Name = "Btn_Bersih"
        Me.Btn_Bersih.Size = New System.Drawing.Size(101, 36)
        Me.Btn_Bersih.TabIndex = 25
        Me.Btn_Bersih.Text = "Bersih"
        Me.Btn_Bersih.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Bersih.UseVisualStyleBackColor = True
        '
        'Txt_Jumlah
        '
        Me.Txt_Jumlah.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_Jumlah.Location = New System.Drawing.Point(227, 230)
        Me.Txt_Jumlah.Name = "Txt_Jumlah"
        Me.Txt_Jumlah.Size = New System.Drawing.Size(159, 26)
        Me.Txt_Jumlah.TabIndex = 21
        '
        'Txt_Posisi
        '
        Me.Txt_Posisi.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_Posisi.Location = New System.Drawing.Point(227, 196)
        Me.Txt_Posisi.Name = "Txt_Posisi"
        Me.Txt_Posisi.Size = New System.Drawing.Size(159, 26)
        Me.Txt_Posisi.TabIndex = 20
        '
        'Txt_Id
        '
        Me.Txt_Id.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_Id.Location = New System.Drawing.Point(227, 72)
        Me.Txt_Id.Name = "Txt_Id"
        Me.Txt_Id.Size = New System.Drawing.Size(159, 26)
        Me.Txt_Id.TabIndex = 18
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(99, 228)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(114, 20)
        Me.Label4.TabIndex = 17
        Me.Label4.Text = "JUMLAH KURSI :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(99, 194)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(111, 20)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "POSISI DUDUK :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(85, 162)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(125, 20)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "NAMA GERBONG :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.SystemColors.Control
        Me.Label1.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(110, 70)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 20)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "ID GERBONG :"
        '
        'Cmb_nama
        '
        Me.Cmb_nama.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cmb_nama.FormattingEnabled = True
        Me.Cmb_nama.Items.AddRange(New Object() {"Darmawangsa", "Tritura", "Pantera"})
        Me.Cmb_nama.Location = New System.Drawing.Point(227, 162)
        Me.Cmb_nama.Name = "Cmb_nama"
        Me.Cmb_nama.Size = New System.Drawing.Size(159, 28)
        Me.Cmb_nama.TabIndex = 28
        '
        'Btn_Keluar
        '
        Me.Btn_Keluar.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Keluar.Image = CType(resources.GetObject("Btn_Keluar.Image"), System.Drawing.Image)
        Me.Btn_Keluar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_Keluar.Location = New System.Drawing.Point(113, 19)
        Me.Btn_Keluar.Name = "Btn_Keluar"
        Me.Btn_Keluar.Size = New System.Drawing.Size(102, 39)
        Me.Btn_Keluar.TabIndex = 89
        Me.Btn_Keluar.Text = "Keluar"
        Me.Btn_Keluar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Keluar.UseVisualStyleBackColor = True
        '
        'Btn_Hapus
        '
        Me.Btn_Hapus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Btn_Hapus.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Hapus.Image = CType(resources.GetObject("Btn_Hapus.Image"), System.Drawing.Image)
        Me.Btn_Hapus.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Hapus.Location = New System.Drawing.Point(114, 73)
        Me.Btn_Hapus.Name = "Btn_Hapus"
        Me.Btn_Hapus.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Btn_Hapus.Size = New System.Drawing.Size(101, 35)
        Me.Btn_Hapus.TabIndex = 88
        Me.Btn_Hapus.Text = "Hapus"
        Me.Btn_Hapus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_Hapus.UseVisualStyleBackColor = True
        '
        'Btn_Ubah
        '
        Me.Btn_Ubah.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Ubah.Image = CType(resources.GetObject("Btn_Ubah.Image"), System.Drawing.Image)
        Me.Btn_Ubah.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_Ubah.Location = New System.Drawing.Point(6, 73)
        Me.Btn_Ubah.Name = "Btn_Ubah"
        Me.Btn_Ubah.Size = New System.Drawing.Size(101, 37)
        Me.Btn_Ubah.TabIndex = 87
        Me.Btn_Ubah.Text = "Ubah"
        Me.Btn_Ubah.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Ubah.UseVisualStyleBackColor = True
        '
        'btn_Simpan
        '
        Me.btn_Simpan.BackColor = System.Drawing.SystemColors.Control
        Me.btn_Simpan.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Simpan.Image = CType(resources.GetObject("btn_Simpan.Image"), System.Drawing.Image)
        Me.btn_Simpan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Simpan.Location = New System.Drawing.Point(6, 19)
        Me.btn_Simpan.Name = "btn_Simpan"
        Me.btn_Simpan.Size = New System.Drawing.Size(101, 39)
        Me.btn_Simpan.TabIndex = 86
        Me.btn_Simpan.Text = "Tambah"
        Me.btn_Simpan.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Simpan.UseVisualStyleBackColor = False
        '
        'txt_idp
        '
        Me.txt_idp.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_idp.Location = New System.Drawing.Point(227, 101)
        Me.txt_idp.Name = "txt_idp"
        Me.txt_idp.Size = New System.Drawing.Size(159, 26)
        Me.txt_idp.TabIndex = 92
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(113, 98)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(97, 20)
        Me.Label5.TabIndex = 91
        Me.Label5.Text = "ID PETUGAS :"
        '
        'txt_namap
        '
        Me.txt_namap.Enabled = False
        Me.txt_namap.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_namap.Location = New System.Drawing.Point(227, 132)
        Me.txt_namap.Name = "txt_namap"
        Me.txt_namap.Size = New System.Drawing.Size(159, 26)
        Me.txt_namap.TabIndex = 94
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(88, 130)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(122, 20)
        Me.Label6.TabIndex = 93
        Me.Label6.Text = "NAMA PETUGAS :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.SystemColors.HighlightText
        Me.Label7.Font = New System.Drawing.Font("Arial Narrow", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(220, 18)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(399, 37)
        Me.Label7.TabIndex = 95
        Me.Label7.Text = "DATA PEMAKAIAN GERBONG" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.White
        Me.GroupBox1.Controls.Add(Me.btn_Simpan)
        Me.GroupBox1.Controls.Add(Me.Btn_Ubah)
        Me.GroupBox1.Controls.Add(Me.Btn_Bersih)
        Me.GroupBox1.Controls.Add(Me.Btn_Hapus)
        Me.GroupBox1.Controls.Add(Me.Btn_Keluar)
        Me.GroupBox1.Location = New System.Drawing.Point(499, 71)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(240, 167)
        Me.GroupBox1.TabIndex = 96
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "OPERASI"
        '
        'formgerbong
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(824, 474)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txt_namap)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txt_idp)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Cmb_nama)
        Me.Controls.Add(Me.Btn_Cari)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.Txt_Jumlah)
        Me.Controls.Add(Me.Txt_Posisi)
        Me.Controls.Add(Me.Txt_Id)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.DoubleBuffered = True
        Me.Name = "formgerbong"
        Me.Text = "Form Gerbong"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Btn_Cari As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Btn_Bersih As System.Windows.Forms.Button
    Friend WithEvents Txt_Jumlah As System.Windows.Forms.TextBox
    Friend WithEvents Txt_Posisi As System.Windows.Forms.TextBox
    Friend WithEvents Txt_Id As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Cmb_nama As System.Windows.Forms.ComboBox
    Friend WithEvents Btn_Keluar As System.Windows.Forms.Button
    Friend WithEvents Btn_Hapus As System.Windows.Forms.Button
    Friend WithEvents Btn_Ubah As System.Windows.Forms.Button
    Friend WithEvents btn_Simpan As System.Windows.Forms.Button
    Friend WithEvents txt_idp As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txt_namap As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox

End Class
