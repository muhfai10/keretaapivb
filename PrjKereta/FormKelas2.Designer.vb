﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormKelas2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormKelas2))
        Me.txt_harga = New System.Windows.Forms.TextBox()
        Me.Btn_Cari = New System.Windows.Forms.Button()
        Me.Txt_Tuju = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cmb_kelas = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Btn_hapus = New System.Windows.Forms.Button()
        Me.Btn_Keluar = New System.Windows.Forms.Button()
        Me.Btn_Cetak = New System.Windows.Forms.Button()
        Me.Btn_bersih = New System.Windows.Forms.Button()
        Me.Btn_ubah = New System.Windows.Forms.Button()
        Me.Btn_tambah = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_jadwal = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_gerbong = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_kelas = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_tiket = New System.Windows.Forms.TextBox()
        Me.LABEL9 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Txt_jml = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Txt_nama = New System.Windows.Forms.TextBox()
        Me.Txt_waktu = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Txt_total = New System.Windows.Forms.TextBox()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txt_harga
        '
        Me.txt_harga.Location = New System.Drawing.Point(247, 297)
        Me.txt_harga.Name = "txt_harga"
        Me.txt_harga.Size = New System.Drawing.Size(200, 20)
        Me.txt_harga.TabIndex = 124
        '
        'Btn_Cari
        '
        Me.Btn_Cari.BackColor = System.Drawing.SystemColors.HighlightText
        Me.Btn_Cari.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Cari.Image = CType(resources.GetObject("Btn_Cari.Image"), System.Drawing.Image)
        Me.Btn_Cari.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_Cari.Location = New System.Drawing.Point(457, 70)
        Me.Btn_Cari.Name = "Btn_Cari"
        Me.Btn_Cari.Size = New System.Drawing.Size(100, 31)
        Me.Btn_Cari.TabIndex = 123
        Me.Btn_Cari.Text = "CARI"
        Me.Btn_Cari.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Cari.UseVisualStyleBackColor = False
        '
        'Txt_Tuju
        '
        Me.Txt_Tuju.Location = New System.Drawing.Point(247, 326)
        Me.Txt_Tuju.Name = "Txt_Tuju"
        Me.Txt_Tuju.Size = New System.Drawing.Size(200, 20)
        Me.Txt_Tuju.TabIndex = 122
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(62, 324)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(60, 20)
        Me.Label8.TabIndex = 121
        Me.Label8.Text = "TUJUAN"
        '
        'cmb_kelas
        '
        Me.cmb_kelas.FormattingEnabled = True
        Me.cmb_kelas.Items.AddRange(New Object() {"Eksekutif", "Bussiness", "Ekonomi"})
        Me.cmb_kelas.Location = New System.Drawing.Point(247, 267)
        Me.cmb_kelas.Name = "cmb_kelas"
        Me.cmb_kelas.Size = New System.Drawing.Size(200, 21)
        Me.cmb_kelas.TabIndex = 120
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.SystemColors.HighlightText
        Me.Label7.Font = New System.Drawing.Font("Arial Narrow", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(227, 9)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(406, 43)
        Me.Label7.TabIndex = 119
        Me.Label7.Text = "DATA PEMAKAIAN KELAS" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.White
        Me.GroupBox1.Controls.Add(Me.Btn_hapus)
        Me.GroupBox1.Controls.Add(Me.Btn_Keluar)
        Me.GroupBox1.Controls.Add(Me.Btn_Cetak)
        Me.GroupBox1.Controls.Add(Me.Btn_bersih)
        Me.GroupBox1.Controls.Add(Me.Btn_ubah)
        Me.GroupBox1.Controls.Add(Me.Btn_tambah)
        Me.GroupBox1.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(586, 70)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(272, 171)
        Me.GroupBox1.TabIndex = 118
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "OPERASI"
        '
        'Btn_hapus
        '
        Me.Btn_hapus.BackColor = System.Drawing.SystemColors.HighlightText
        Me.Btn_hapus.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_hapus.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Btn_hapus.Image = CType(resources.GetObject("Btn_hapus.Image"), System.Drawing.Image)
        Me.Btn_hapus.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_hapus.Location = New System.Drawing.Point(145, 73)
        Me.Btn_hapus.Name = "Btn_hapus"
        Me.Btn_hapus.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Btn_hapus.Size = New System.Drawing.Size(103, 37)
        Me.Btn_hapus.TabIndex = 94
        Me.Btn_hapus.Text = "Hapus"
        Me.Btn_hapus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_hapus.UseVisualStyleBackColor = False
        '
        'Btn_Keluar
        '
        Me.Btn_Keluar.BackColor = System.Drawing.SystemColors.HighlightText
        Me.Btn_Keluar.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Keluar.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Btn_Keluar.Image = CType(resources.GetObject("Btn_Keluar.Image"), System.Drawing.Image)
        Me.Btn_Keluar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_Keluar.Location = New System.Drawing.Point(20, 115)
        Me.Btn_Keluar.Name = "Btn_Keluar"
        Me.Btn_Keluar.Size = New System.Drawing.Size(101, 42)
        Me.Btn_Keluar.TabIndex = 95
        Me.Btn_Keluar.Text = "Keluar"
        Me.Btn_Keluar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Keluar.UseVisualStyleBackColor = False
        '
        'Btn_Cetak
        '
        Me.Btn_Cetak.BackColor = System.Drawing.SystemColors.HighlightText
        Me.Btn_Cetak.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Cetak.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Btn_Cetak.Image = CType(resources.GetObject("Btn_Cetak.Image"), System.Drawing.Image)
        Me.Btn_Cetak.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_Cetak.Location = New System.Drawing.Point(145, 114)
        Me.Btn_Cetak.Name = "Btn_Cetak"
        Me.Btn_Cetak.Size = New System.Drawing.Size(103, 43)
        Me.Btn_Cetak.TabIndex = 53
        Me.Btn_Cetak.Text = "Cetak"
        Me.Btn_Cetak.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Cetak.UseVisualStyleBackColor = False
        '
        'Btn_bersih
        '
        Me.Btn_bersih.BackColor = System.Drawing.SystemColors.HighlightText
        Me.Btn_bersih.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_bersih.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Btn_bersih.Image = CType(resources.GetObject("Btn_bersih.Image"), System.Drawing.Image)
        Me.Btn_bersih.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_bersih.Location = New System.Drawing.Point(145, 27)
        Me.Btn_bersih.Name = "Btn_bersih"
        Me.Btn_bersih.Size = New System.Drawing.Size(101, 37)
        Me.Btn_bersih.TabIndex = 91
        Me.Btn_bersih.Text = "Bersih"
        Me.Btn_bersih.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_bersih.UseVisualStyleBackColor = False
        '
        'Btn_ubah
        '
        Me.Btn_ubah.BackColor = System.Drawing.SystemColors.HighlightText
        Me.Btn_ubah.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_ubah.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Btn_ubah.Image = CType(resources.GetObject("Btn_ubah.Image"), System.Drawing.Image)
        Me.Btn_ubah.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_ubah.Location = New System.Drawing.Point(20, 73)
        Me.Btn_ubah.Name = "Btn_ubah"
        Me.Btn_ubah.Size = New System.Drawing.Size(101, 37)
        Me.Btn_ubah.TabIndex = 93
        Me.Btn_ubah.Text = "Ubah"
        Me.Btn_ubah.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_ubah.UseVisualStyleBackColor = False
        '
        'Btn_tambah
        '
        Me.Btn_tambah.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar
        Me.Btn_tambah.BackColor = System.Drawing.SystemColors.HighlightText
        Me.Btn_tambah.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_tambah.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Btn_tambah.Image = CType(resources.GetObject("Btn_tambah.Image"), System.Drawing.Image)
        Me.Btn_tambah.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_tambah.Location = New System.Drawing.Point(20, 25)
        Me.Btn_tambah.Name = "Btn_tambah"
        Me.Btn_tambah.Size = New System.Drawing.Size(101, 39)
        Me.Btn_tambah.TabIndex = 92
        Me.Btn_tambah.Text = "Tambah"
        Me.Btn_tambah.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_tambah.UseVisualStyleBackColor = False
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(12, 391)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(857, 137)
        Me.DataGridView1.TabIndex = 117
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(64, 293)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(55, 20)
        Me.Label6.TabIndex = 114
        Me.Label6.Text = "HARGA"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(62, 265)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(98, 20)
        Me.Label1.TabIndex = 113
        Me.Label1.Text = "JENIS_KELAS"
        '
        'txt_jadwal
        '
        Me.txt_jadwal.Location = New System.Drawing.Point(247, 212)
        Me.txt_jadwal.Name = "txt_jadwal"
        Me.txt_jadwal.Size = New System.Drawing.Size(200, 20)
        Me.txt_jadwal.TabIndex = 112
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(62, 213)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(82, 20)
        Me.Label3.TabIndex = 111
        Me.Label3.Text = "ID_JADWAL"
        '
        'txt_gerbong
        '
        Me.txt_gerbong.Location = New System.Drawing.Point(247, 157)
        Me.txt_gerbong.Name = "txt_gerbong"
        Me.txt_gerbong.Size = New System.Drawing.Size(200, 20)
        Me.txt_gerbong.TabIndex = 110
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(62, 156)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(95, 20)
        Me.Label4.TabIndex = 109
        Me.Label4.Text = "ID_GERBONG"
        '
        'txt_kelas
        '
        Me.txt_kelas.Location = New System.Drawing.Point(247, 74)
        Me.txt_kelas.Multiline = True
        Me.txt_kelas.Name = "txt_kelas"
        Me.txt_kelas.Size = New System.Drawing.Size(200, 23)
        Me.txt_kelas.TabIndex = 108
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(62, 72)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 20)
        Me.Label2.TabIndex = 107
        Me.Label2.Text = "ID_KELAS"
        '
        'txt_tiket
        '
        Me.txt_tiket.Location = New System.Drawing.Point(247, 103)
        Me.txt_tiket.Name = "txt_tiket"
        Me.txt_tiket.Size = New System.Drawing.Size(200, 20)
        Me.txt_tiket.TabIndex = 106
        '
        'LABEL9
        '
        Me.LABEL9.AutoSize = True
        Me.LABEL9.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LABEL9.Location = New System.Drawing.Point(62, 101)
        Me.LABEL9.Name = "LABEL9"
        Me.LABEL9.Size = New System.Drawing.Size(67, 20)
        Me.LABEL9.TabIndex = 105
        Me.LABEL9.Text = "ID_TIKET"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(62, 128)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(107, 20)
        Me.Label5.TabIndex = 125
        Me.Label5.Text = "JUMLAH_TIKET"
        '
        'Txt_jml
        '
        Me.Txt_jml.Enabled = False
        Me.Txt_jml.Location = New System.Drawing.Point(247, 130)
        Me.Txt_jml.Name = "Txt_jml"
        Me.Txt_jml.Size = New System.Drawing.Size(200, 20)
        Me.Txt_jml.TabIndex = 126
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(62, 186)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(120, 20)
        Me.Label10.TabIndex = 127
        Me.Label10.Text = "NAMA_GERBONG"
        '
        'Txt_nama
        '
        Me.Txt_nama.Enabled = False
        Me.Txt_nama.Location = New System.Drawing.Point(247, 184)
        Me.Txt_nama.Name = "Txt_nama"
        Me.Txt_nama.Size = New System.Drawing.Size(200, 20)
        Me.Txt_nama.TabIndex = 128
        '
        'Txt_waktu
        '
        Me.Txt_waktu.Enabled = False
        Me.Txt_waktu.Location = New System.Drawing.Point(247, 240)
        Me.Txt_waktu.Name = "Txt_waktu"
        Me.Txt_waktu.Size = New System.Drawing.Size(200, 20)
        Me.Txt_waktu.TabIndex = 129
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(62, 240)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(179, 20)
        Me.Label11.TabIndex = 130
        Me.Label11.Text = "WAKTU_KEBERANGKATAN"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(62, 354)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(105, 20)
        Me.Label12.TabIndex = 131
        Me.Label12.Text = "TOTAL_HARGA"
        '
        'Txt_total
        '
        Me.Txt_total.Enabled = False
        Me.Txt_total.Location = New System.Drawing.Point(247, 356)
        Me.Txt_total.Name = "Txt_total"
        Me.Txt_total.Size = New System.Drawing.Size(200, 20)
        Me.Txt_total.TabIndex = 132
        '
        'FormKelas2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(890, 540)
        Me.Controls.Add(Me.Txt_total)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Txt_waktu)
        Me.Controls.Add(Me.Txt_nama)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Txt_jml)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txt_harga)
        Me.Controls.Add(Me.Btn_Cari)
        Me.Controls.Add(Me.Txt_Tuju)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.cmb_kelas)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txt_jadwal)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txt_gerbong)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txt_kelas)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txt_tiket)
        Me.Controls.Add(Me.LABEL9)
        Me.Name = "FormKelas2"
        Me.Text = "FormKelas2"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txt_harga As System.Windows.Forms.TextBox
    Friend WithEvents Btn_Cari As System.Windows.Forms.Button
    Friend WithEvents Txt_Tuju As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cmb_kelas As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Btn_hapus As System.Windows.Forms.Button
    Friend WithEvents Btn_Keluar As System.Windows.Forms.Button
    Friend WithEvents Btn_Cetak As System.Windows.Forms.Button
    Friend WithEvents Btn_bersih As System.Windows.Forms.Button
    Friend WithEvents Btn_ubah As System.Windows.Forms.Button
    Friend WithEvents Btn_tambah As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txt_jadwal As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txt_gerbong As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txt_kelas As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txt_tiket As System.Windows.Forms.TextBox
    Friend WithEvents LABEL9 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Txt_jml As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Txt_nama As System.Windows.Forms.TextBox
    Friend WithEvents Txt_waktu As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Txt_total As System.Windows.Forms.TextBox
End Class
