﻿Public Class formgerbong
    Private conn As New MySqlConnection
    Dim connString = "Database=kereta_api; Data source=localhost; user id=root; password=;"
    Private Sub FormKereta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        TampilkanDaftar()
    End Sub

    Private Sub Bersihkan()
        Txt_Id.Text = ""
        txt_idp.Text = ""
        Cmb_nama.Text = ""
        Txt_Posisi.Text = ""
        Txt_Jumlah.Text = ""
        Txt_Id.Focus()
        Btn_Simpan.Enabled = True
        Btn_Ubah.Enabled = True
        Btn_Hapus.Enabled = True
    End Sub


    Private Sub Btn_Cari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Cari.Click
        CariByNIM()
    End Sub

    Private Sub Btn_Bersih_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Bersih.Click
        Bersihkan()
    End Sub

    Private Sub CariByNIM()
        Dim connString = "Database=kereta_api; Data source=localhost; user id=root; password=;"
        conn = New MySqlConnection(connString)
        conn.Open()

        Dim dr As MySqlDataReader = Nothing
        Dim cmdSelect As MySqlCommand = conn.CreateCommand

        Try
            If Txt_Id.Text <> "" Then
                Dim sqlString As String = "select * from t_gerbong where id_gerbong=@id_gerbong;"
                cmdSelect.Parameters.Add("@id_gerbong", MySqlDbType.String, 20).Value = Txt_Id.Text
                cmdSelect.CommandText = sqlString
                dr = cmdSelect.ExecuteReader

                If dr.Read() Then
                    Txt_Id.Text = dr("id_gerbong").ToString
                    txt_idp.Text = dr("id_petugas").ToString
                    Cmb_nama.Text = dr("nama_gerbong").ToString
                    Txt_Posisi.Text = dr("posisi_duduk").ToString
                    Txt_Jumlah.Text = dr("jumlah_kursi").ToString
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Terjadi kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            cmdSelect.Dispose()
            'conn.Close()
            conn = Nothing
        End Try
    End Sub

    Private Sub TampilkanDaftar()
        Dim da As New MySqlDataAdapter()
        Dim ds As New DataSet()
        Dim connString = "Database=kereta_api; Data source=localhost; user id=root; password=;"
        conn = New MySqlConnection(connString)
        Dim cmdSelect As MySqlCommand = conn.CreateCommand()
        Dim sqlString As String
        Try
            If Txt_Id.Text <> "" Then
                sqlString = "select * from vw_data_gerbong where id_gerbong=@id_gerbong;"
                cmdSelect.Parameters.Add("@kode", MySqlDbType.String, 10).Value = Txt_Id.Text
            Else
                sqlString = "select * from vw_data_gerbong;"
            End If

            cmdSelect.CommandText = sqlString

            da.SelectCommand = cmdSelect
            da.Fill(ds, "vw_data_gerbong")
            DataGridView1.DataSource = ds
            DataGridView1.DataMember = "vw_data_gerbong"
            DataGridView1.ReadOnly = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Terjadi kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            cmdSelect.Dispose()
            conn.Close()
            conn = Nothing
        End Try
    End Sub

    Private Sub Cmb_nama_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cmb_nama.SelectedIndexChanged
        If Cmb_nama.Text = "Darmawangsa" Then
            Txt_Jumlah.Text = "40"
        ElseIf Cmb_nama.Text = "Tritura" Then
            Txt_Jumlah.Text = "50"
        Else
            Txt_Jumlah.Text = "60"
        End If
    End Sub

    Private Sub txt_idp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_idp.TextChanged
        Dim connString = "Database=kereta_api;data source=localhost;user id=root;password=;"
        conn = New MySqlConnection(connString)
        conn.Open()

        Dim dr As MySqlDataReader = Nothing
        Dim cmdSelect As MySqlCommand = conn.CreateCommand()
        Try
            If txt_idp.Text <> "" Then
                Dim sqlString As String = "Select * from t_petugas where id_petugas=@id_petugas;"
                cmdSelect.Parameters.Add("@id_petugas", MySqlDbType.String, 20).Value = txt_idp.Text
                cmdSelect.CommandText = sqlString
                dr = cmdSelect.ExecuteReader

                If dr.Read() Then
                    txt_idp.Text = dr("id_petugas").ToString
                    txt_namap.Text = dr("nama").ToString
                Else

                    txt_namap.Text = ""
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Terjadi Kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            cmdSelect.Dispose()
            'conn.Close()
            conn = Nothing
        End Try
    End Sub

    Private Sub Btn_Keluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Keluar.Click
        Me.Close()
    End Sub

    Private Sub btn_Simpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Simpan.Click
        Dim connString = "Database=kereta_api; Data source=localhost; user id=root; password=;"
        conn = New MySqlConnection(connString)
        conn.Open()

        Dim cmdInsert As MySqlCommand = conn.CreateCommand

        If Txt_Id.Text <> "" Then
            Try
                Dim sqlString As String = "insert into t_gerbong (id_gerbong, id_petugas,nama_gerbong, posisi_duduk, jumlah_kursi) values (@id_gerbong, @id_petugas,@nama_gerbong, @posisi_duduk, @jumlah_kursi);"
                cmdInsert.CommandText = sqlString
                cmdInsert.Connection = conn
                cmdInsert.Parameters.Add("@id_gerbong", MySqlDbType.String, 20).Value = Txt_Id.Text
                cmdInsert.Parameters.Add("@id_petugas", MySqlDbType.String, 20).Value = txt_idp.Text
                cmdInsert.Parameters.Add("@nama_gerbong", MySqlDbType.String, 25).Value = Cmb_nama.Text
                cmdInsert.Parameters.Add("@posisi_duduk", MySqlDbType.VarChar, 25).Value = Txt_Posisi.Text
                cmdInsert.Parameters.Add("@jumlah_kursi", MySqlDbType.Int16, 5).Value = Txt_Jumlah.Text
                cmdInsert.ExecuteNonQuery()
                MessageBox.Show("Data berhasil ditambahkan", "My Project", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Bersihkan()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Terjadi kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Finally
                cmdInsert.Dispose()
                'conn.Close()
                conn = Nothing
            End Try
        End If
        TampilkanDaftar()
    End Sub

    Private Sub Btn_Simpan_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub Btn_Ubah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Ubah.Click
        Dim connString = "Database=kereta_api; Data source=localhost; user id=root; password=;"
        conn = New MySqlConnection(connString)
        conn.Open()

        Dim cmdUpdate As MySqlCommand = conn.CreateCommand

        If Txt_Id.Text <> "" Then
            Try
                Dim sqlString As String = "update t_gerbong set id_petugas=@id_petugas,nama_gerbong=@nama_gerbong, posisi_duduk=@posisi_duduk, jumlah_kursi=@jumlah_kursi where id_gerbong=@id_gerbong;"
                cmdUpdate.CommandText = sqlString
                cmdUpdate.Connection = conn
                cmdUpdate.Parameters.Add("@id_gerbong", MySqlDbType.String, 20).Value = Txt_Id.Text
                cmdUpdate.Parameters.Add("@id_petugas", MySqlDbType.String, 20).Value = txt_idp.Text
                cmdUpdate.Parameters.Add("@nama_gerbong", MySqlDbType.String, 25).Value = Cmb_nama.Text
                cmdUpdate.Parameters.Add("@posisi_duduk", MySqlDbType.VarChar, 25).Value = Txt_Posisi.Text
                cmdUpdate.Parameters.Add("@jumlah_kursi", MySqlDbType.Int16, 5).Value = Txt_Jumlah.Text
                cmdUpdate.ExecuteNonQuery()
                MessageBox.Show("data berhasil diubah", "My Project", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Bersihkan()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Terjadi kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Finally
                cmdUpdate.Dispose()
                'conn.Close()
                conn = Nothing
            End Try
        End If
        TampilkanDaftar()
    End Sub

    Private Sub Btn_Hapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Hapus.Click
        Dim connString = "Database=kereta_api; Data source=localhost; user id=root; password=;"
        conn = New MySqlConnection(connString)
        conn.Open()

        Dim cmdDelete As MySqlCommand = conn.CreateCommand

        If Txt_Id.Text <> "" Then
            If MessageBox.Show("Anda yakin data gerbong dengan Id_gerbong " & Txt_Id.Text & " akan dihapus ?", "Peringatan!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                Try
                    Dim sqlString As String = "delete from t_gerbong where id_gerbong=@id_gerbong;"
                    cmdDelete.CommandText = sqlString
                    cmdDelete.Connection = conn
                    cmdDelete.Parameters.Add("@id_gerbong", MySqlDbType.String, 5).Value = Txt_Id.Text
                    cmdDelete.ExecuteNonQuery()
                    MessageBox.Show("Data berhasil dihapus", "My Project", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Bersihkan()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Terjadi kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Finally
                    cmdDelete.Dispose()
                    'conn.Close()
                    conn = Nothing
                End Try
            End If
            TampilkanDaftar()
        End If
    End Sub
End Class


