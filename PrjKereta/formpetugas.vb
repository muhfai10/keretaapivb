﻿Public Class formpetugas

    Private Sub formpetugas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        TampilkanDaftar()
    End Sub

    Private conn As New MySqlConnection
    Private Sub Bersihkan()
        txt_idpetugas.Text = ""
        txt_nama.Text = ""
        DateTimePicker1.Text = ""
        Cmb_jk.Text = ""
        Txt_telp.Text = ""
        txt_idpetugas.Focus()
        Btn_Tambah.Enabled = True
        Btn_Ubah.Enabled = True
        Btn_Hapus.Enabled = True
    End Sub

    Private Sub CariByNIM()
        Dim connString = "Database=kereta_api; Data source=localhost; user id=root; password=;"
        conn = New MySqlConnection(connString)
        conn.Open()

        Dim dr As MySqlDataReader = Nothing
        Dim cmdSelect As MySqlCommand = conn.CreateCommand

        Try
            If txt_idpetugas.Text <> "" Then
                Dim sqlString As String = "select * from t_petugas where id_petugas=@id_petugas;"
                cmdSelect.Parameters.Add("@id_petugas", MySqlDbType.String, 5).Value = txt_idpetugas.Text
                cmdSelect.CommandText = sqlString
                dr = cmdSelect.ExecuteReader

                If dr.Read() Then
                    txt_idpetugas.Text = dr("id_petugas").ToString
                    txt_nama.Text = dr("nama").ToString
                    Txt_telp.Text = dr("no_telp").ToString
                    Cmb_jk.Text = dr("jenis_kelamin").ToString
                    DateTimePicker1.Text = dr("tgl_lahir").ToString
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Terjadi kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            cmdSelect.Dispose()
            conn.Close()
            conn = Nothing
        End Try
    End Sub

    Private Sub TampilkanDaftar()
        Dim da As New MySqlDataAdapter()
        Dim ds As New DataSet()

        Dim connString = "Database=kereta_api; Data source=localhost; user id=root; password=;"
        conn = New MySqlConnection(connString)


        Dim cmdSelect As MySqlCommand = conn.CreateCommand()
        Dim sqlString As String
        Try
            If txt_idpetugas.Text <> "" Then
                sqlString = "select * from t_petugas where id_petugas=@id_petugas;"
                cmdSelect.Parameters.Add("@kode", MySqlDbType.String, 10).Value = txt_idpetugas.Text
            Else
                sqlString = "select * from t_petugas;"
            End If

            cmdSelect.CommandText = sqlString

            da.SelectCommand = cmdSelect
            da.Fill(ds, "t_petugas")
            DataGridView1.DataSource = ds
            DataGridView1.DataMember = "t_petugas"
            DataGridView1.ReadOnly = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Terjadi kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            cmdSelect.Dispose()
            conn.Close()
            conn = Nothing
        End Try
    End Sub

    Private Sub Btn_Cetak_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Cetak.Click
        Dim connString = "Database=kereta_api; Data source=localhost; user id=root; password=;"
        Dim da As New MySqlDataAdapter
        Dim ds As New DataSet
        Dim sqlString As String
        conn = New MySqlConnection(connString)
        Dim cmd As MySqlCommand = conn.CreateCommand
        conn.Open()

        Try
            ds.Clear()
            sqlString = "select * from t_petugas;"

            cmd = New MySqlCommand(sqlString, conn)
            da.SelectCommand = cmd
            da.Fill(ds, "t_petugas")

            Dim laporan As New rptDataPetugas
            laporan.SetDataSource(ds)
            frmViewLaporan.CrystalReportViewer1.ReportSource = laporan
            frmViewLaporan.ShowDialog()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Terjadi Kegagalan !", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            conn.Close()
            conn = Nothing
        End Try
    End Sub

    Private Sub Btn_Ubah_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Ubah.Click
        Dim connString = "Database=kereta_api; Data source=localhost; user id=root; password=;"
        conn = New MySqlConnection(connString)
        conn.Open()

        Dim cmdUpdate As MySqlCommand = conn.CreateCommand

        If txt_idpetugas.Text <> "" Then
            Try
                Dim sqlString As String = "update t_petugas set nama=@nama, tgl_lahir=@tgl_lahir, jenis_kelamin=@jenis_kelamin, no_telp=@no_telp where id_petugas=@id_petugas;"
                cmdUpdate.CommandText = sqlString
                cmdUpdate.Connection = conn
                cmdUpdate.Parameters.Add("@id_petugas", MySqlDbType.String, 8).Value = txt_idpetugas.Text
                cmdUpdate.Parameters.Add("@nama", MySqlDbType.String, 25).Value = txt_nama.Text
                cmdUpdate.Parameters.Add("@tgl_lahir", MySqlDbType.Date).Value = DateTimePicker1.Text
                cmdUpdate.Parameters.Add("@jenis_kelamin", MySqlDbType.String, 25).Value = Cmb_jk.Text
                cmdUpdate.Parameters.Add("@no_telp", MySqlDbType.String, 25).Value = Txt_telp.Text
                cmdUpdate.ExecuteNonQuery()
                MessageBox.Show("data berhasil diubah", "My Project", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Bersihkan()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Terjadi kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Finally
                cmdUpdate.Dispose()
                conn.Close()
                conn = Nothing
            End Try
        End If
        TampilkanDaftar()
    End Sub

    Private Sub Btn_Tambah_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Tambah.Click
        Dim connString = "Database=kereta_api; Data source=localhost; user id=root; password=;"
        conn = New MySqlConnection(connString)
        conn.Open()

        Dim cmdInsert As MySqlCommand = conn.CreateCommand

        If txt_idpetugas.Text <> "" Then
            Try
                Dim sqlString As String = "insert into t_petugas (id_petugas, nama, tgl_lahir, jenis_kelamin, no_telp) values (@id_petugas, @nama, @tgl_lahir, @jenis_kelamin, @no_telp);"
                cmdInsert.CommandText = sqlString
                cmdInsert.Connection = conn
                cmdInsert.Parameters.Add("@id_petugas", MySqlDbType.String, 8).Value = txt_idpetugas.Text
                cmdInsert.Parameters.Add("@nama", MySqlDbType.String, 25).Value = txt_nama.Text
                cmdInsert.Parameters.Add("@tgl_lahir", MySqlDbType.Date).Value = DateTimePicker1.Text
                cmdInsert.Parameters.Add("@jenis_kelamin", MySqlDbType.String, 25).Value = Cmb_jk.Text
                cmdInsert.Parameters.Add("@no_telp", MySqlDbType.String, 25).Value = Txt_telp.Text
                cmdInsert.ExecuteNonQuery()
                MessageBox.Show("Data berhasil ditambahkan", "My Project", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Bersihkan()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Terjadi kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Finally
                cmdInsert.Dispose()
                conn.Close()
                conn = Nothing
            End Try
        End If
        TampilkanDaftar()
    End Sub

    Private Sub Btn_Keluar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Keluar.Click
        Me.Close()
    End Sub

    Private Sub Btn_Hapus_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Hapus.Click
        Dim connString = "Database=kereta_api; Data source=localhost; user id=root; password=;"
        conn = New MySqlConnection(connString)
        conn.Open()

        Dim cmdDelete As MySqlCommand = conn.CreateCommand

        If txt_idpetugas.Text <> "" Then
            If MessageBox.Show("Anda yakin data Pelanggan dengan id petugas " & txt_idpetugas.Text & " akan dihapus ?", "Peringatan!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                Try
                    Dim sqlString As String = "delete from t_petugas where id_petugas=@id_petugas;"
                    cmdDelete.CommandText = sqlString
                    cmdDelete.Connection = conn
                    cmdDelete.Parameters.Add("@id_petugas", MySqlDbType.String, 8).Value = txt_idpetugas.Text
                    cmdDelete.ExecuteNonQuery()
                    MessageBox.Show("Data berhasil dihapus", "My Project", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Bersihkan()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Terjadi kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Finally
                    cmdDelete.Dispose()
                    'conn.Close()
                    conn = Nothing
                End Try
            Else
                MessageBox.Show("Data Tidak Jadi di Hapus")
            End If
            TampilkanDaftar()
        End If
    End Sub

    Private Sub Btn_bersih_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_bersih.Click
        Bersihkan()
    End Sub

    Private Sub Btn_Cari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Cari.Click
        CariByNIM()
    End Sub

    Private Sub GroupBox1_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox1.Enter

    End Sub
End Class
