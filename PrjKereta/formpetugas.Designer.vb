﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class formpetugas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(formpetugas))
        Me.Txt_telp = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.txt_nama = New System.Windows.Forms.TextBox()
        Me.LABEL5 = New System.Windows.Forms.Label()
        Me.LABEL6 = New System.Windows.Forms.Label()
        Me.txt_idpetugas = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Btn_Cetak = New System.Windows.Forms.Button()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Btn_Simpan = New System.Windows.Forms.Button()
        Me.Cmb_jk = New System.Windows.Forms.ComboBox()
        Me.txt_alamat = New System.Windows.Forms.TextBox()
        Me.Btn_Cari = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Btn_Keluar = New System.Windows.Forms.Button()
        Me.Btn_Hapus = New System.Windows.Forms.Button()
        Me.Btn_Ubah = New System.Windows.Forms.Button()
        Me.Btn_Tambah = New System.Windows.Forms.Button()
        Me.Btn_bersih = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Txt_telp
        '
        Me.Txt_telp.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_telp.Location = New System.Drawing.Point(216, 204)
        Me.Txt_telp.Name = "Txt_telp"
        Me.Txt_telp.Size = New System.Drawing.Size(154, 26)
        Me.Txt_telp.TabIndex = 50
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(99, 203)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(66, 20)
        Me.Label4.TabIndex = 49
        Me.Label4.Text = "NO TELP"
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(97, 254)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(555, 169)
        Me.DataGridView1.TabIndex = 44
        '
        'txt_nama
        '
        Me.txt_nama.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_nama.Location = New System.Drawing.Point(216, 112)
        Me.txt_nama.Name = "txt_nama"
        Me.txt_nama.Size = New System.Drawing.Size(154, 26)
        Me.txt_nama.TabIndex = 36
        '
        'LABEL5
        '
        Me.LABEL5.AutoSize = True
        Me.LABEL5.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LABEL5.Location = New System.Drawing.Point(99, 171)
        Me.LABEL5.Name = "LABEL5"
        Me.LABEL5.Size = New System.Drawing.Size(81, 20)
        Me.LABEL5.TabIndex = 34
        Me.LABEL5.Text = "TGL_LAHIR"
        '
        'LABEL6
        '
        Me.LABEL6.AutoSize = True
        Me.LABEL6.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LABEL6.Location = New System.Drawing.Point(99, 116)
        Me.LABEL6.Name = "LABEL6"
        Me.LABEL6.Size = New System.Drawing.Size(47, 20)
        Me.LABEL6.TabIndex = 33
        Me.LABEL6.Text = "NAMA"
        '
        'txt_idpetugas
        '
        Me.txt_idpetugas.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_idpetugas.Location = New System.Drawing.Point(216, 82)
        Me.txt_idpetugas.Name = "txt_idpetugas"
        Me.txt_idpetugas.Size = New System.Drawing.Size(154, 26)
        Me.txt_idpetugas.TabIndex = 52
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(99, 84)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(92, 20)
        Me.Label7.TabIndex = 51
        Me.Label7.Text = "ID_PETUGAS"
        '
        'Btn_Cetak
        '
        Me.Btn_Cetak.BackColor = System.Drawing.SystemColors.Info
        Me.Btn_Cetak.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Cetak.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Btn_Cetak.Image = CType(resources.GetObject("Btn_Cetak.Image"), System.Drawing.Image)
        Me.Btn_Cetak.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_Cetak.Location = New System.Drawing.Point(20, 115)
        Me.Btn_Cetak.Name = "Btn_Cetak"
        Me.Btn_Cetak.Size = New System.Drawing.Size(103, 43)
        Me.Btn_Cetak.TabIndex = 53
        Me.Btn_Cetak.Text = "Cetak"
        Me.Btn_Cetak.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Cetak.UseVisualStyleBackColor = False
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(215, 174)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(155, 26)
        Me.DateTimePicker1.TabIndex = 55
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(99, 144)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(110, 20)
        Me.Label1.TabIndex = 56
        Me.Label1.Text = "JENIS KELAMIN"
        '
        'Btn_Simpan
        '
        Me.Btn_Simpan.Location = New System.Drawing.Point(422, 19)
        Me.Btn_Simpan.Name = "Btn_Simpan"
        Me.Btn_Simpan.Size = New System.Drawing.Size(75, 23)
        Me.Btn_Simpan.TabIndex = 45
        Me.Btn_Simpan.Text = "TAMBAH"
        Me.Btn_Simpan.UseVisualStyleBackColor = True
        '
        'Cmb_jk
        '
        Me.Cmb_jk.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cmb_jk.FormattingEnabled = True
        Me.Cmb_jk.Items.AddRange(New Object() {"Laki-Laki", "Perempuan"})
        Me.Cmb_jk.Location = New System.Drawing.Point(216, 141)
        Me.Cmb_jk.Name = "Cmb_jk"
        Me.Cmb_jk.Size = New System.Drawing.Size(154, 28)
        Me.Cmb_jk.TabIndex = 57
        '
        'txt_alamat
        '
        Me.txt_alamat.Location = New System.Drawing.Point(210, 124)
        Me.txt_alamat.Name = "txt_alamat"
        Me.txt_alamat.Size = New System.Drawing.Size(154, 20)
        Me.txt_alamat.TabIndex = 50
        '
        'Btn_Cari
        '
        Me.Btn_Cari.BackColor = System.Drawing.Color.AliceBlue
        Me.Btn_Cari.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Cari.Image = CType(resources.GetObject("Btn_Cari.Image"), System.Drawing.Image)
        Me.Btn_Cari.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_Cari.Location = New System.Drawing.Point(376, 81)
        Me.Btn_Cari.Name = "Btn_Cari"
        Me.Btn_Cari.Size = New System.Drawing.Size(75, 27)
        Me.Btn_Cari.TabIndex = 58
        Me.Btn_Cari.Text = "CARI"
        Me.Btn_Cari.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Cari.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.SystemColors.HighlightText
        Me.Label2.Font = New System.Drawing.Font("Arial Narrow", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(244, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(225, 37)
        Me.Label2.TabIndex = 81
        Me.Label2.Text = "DATA PETUGAS"
        '
        'Btn_Keluar
        '
        Me.Btn_Keluar.BackColor = System.Drawing.SystemColors.Info
        Me.Btn_Keluar.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Keluar.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Btn_Keluar.Image = CType(resources.GetObject("Btn_Keluar.Image"), System.Drawing.Image)
        Me.Btn_Keluar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_Keluar.Location = New System.Drawing.Point(145, 116)
        Me.Btn_Keluar.Name = "Btn_Keluar"
        Me.Btn_Keluar.Size = New System.Drawing.Size(103, 42)
        Me.Btn_Keluar.TabIndex = 95
        Me.Btn_Keluar.Text = "Keluar"
        Me.Btn_Keluar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Keluar.UseVisualStyleBackColor = False
        '
        'Btn_Hapus
        '
        Me.Btn_Hapus.BackColor = System.Drawing.SystemColors.Info
        Me.Btn_Hapus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Btn_Hapus.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Hapus.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Btn_Hapus.Image = CType(resources.GetObject("Btn_Hapus.Image"), System.Drawing.Image)
        Me.Btn_Hapus.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Hapus.Location = New System.Drawing.Point(145, 73)
        Me.Btn_Hapus.Name = "Btn_Hapus"
        Me.Btn_Hapus.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Btn_Hapus.Size = New System.Drawing.Size(103, 37)
        Me.Btn_Hapus.TabIndex = 94
        Me.Btn_Hapus.Text = "Hapus"
        Me.Btn_Hapus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_Hapus.UseVisualStyleBackColor = False
        '
        'Btn_Ubah
        '
        Me.Btn_Ubah.BackColor = System.Drawing.SystemColors.Info
        Me.Btn_Ubah.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Ubah.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Btn_Ubah.Image = CType(resources.GetObject("Btn_Ubah.Image"), System.Drawing.Image)
        Me.Btn_Ubah.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_Ubah.Location = New System.Drawing.Point(20, 72)
        Me.Btn_Ubah.Name = "Btn_Ubah"
        Me.Btn_Ubah.Size = New System.Drawing.Size(101, 37)
        Me.Btn_Ubah.TabIndex = 93
        Me.Btn_Ubah.Text = "Ubah"
        Me.Btn_Ubah.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Ubah.UseVisualStyleBackColor = False
        '
        'Btn_Tambah
        '
        Me.Btn_Tambah.BackColor = System.Drawing.SystemColors.Info
        Me.Btn_Tambah.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Tambah.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Btn_Tambah.Image = CType(resources.GetObject("Btn_Tambah.Image"), System.Drawing.Image)
        Me.Btn_Tambah.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_Tambah.Location = New System.Drawing.Point(20, 25)
        Me.Btn_Tambah.Name = "Btn_Tambah"
        Me.Btn_Tambah.Size = New System.Drawing.Size(101, 39)
        Me.Btn_Tambah.TabIndex = 92
        Me.Btn_Tambah.Text = "Tambah"
        Me.Btn_Tambah.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Tambah.UseVisualStyleBackColor = False
        '
        'Btn_bersih
        '
        Me.Btn_bersih.BackColor = System.Drawing.SystemColors.Info
        Me.Btn_bersih.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_bersih.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Btn_bersih.Image = CType(resources.GetObject("Btn_bersih.Image"), System.Drawing.Image)
        Me.Btn_bersih.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_bersih.Location = New System.Drawing.Point(145, 27)
        Me.Btn_bersih.Name = "Btn_bersih"
        Me.Btn_bersih.Size = New System.Drawing.Size(103, 37)
        Me.Btn_bersih.TabIndex = 91
        Me.Btn_bersih.Text = "Bersih"
        Me.Btn_bersih.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_bersih.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.GhostWhite
        Me.GroupBox1.Controls.Add(Me.Btn_Hapus)
        Me.GroupBox1.Controls.Add(Me.Btn_Keluar)
        Me.GroupBox1.Controls.Add(Me.Btn_Cetak)
        Me.GroupBox1.Controls.Add(Me.Btn_bersih)
        Me.GroupBox1.Controls.Add(Me.Btn_Ubah)
        Me.GroupBox1.Controls.Add(Me.Btn_Tambah)
        Me.GroupBox1.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(473, 81)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(272, 167)
        Me.GroupBox1.TabIndex = 96
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "OPERASI"
        '
        'formpetugas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(911, 426)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Btn_Cari)
        Me.Controls.Add(Me.Cmb_jk)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.txt_idpetugas)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Txt_telp)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.txt_nama)
        Me.Controls.Add(Me.LABEL5)
        Me.Controls.Add(Me.LABEL6)
        Me.Name = "formpetugas"
        Me.Text = "formpetugas"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Txt_telp As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents txt_nama As System.Windows.Forms.TextBox
    Friend WithEvents LABEL5 As System.Windows.Forms.Label
    Friend WithEvents LABEL6 As System.Windows.Forms.Label
    Friend WithEvents txt_idpetugas As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Btn_Cetak As System.Windows.Forms.Button
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Txt_jk As System.Windows.Forms.TextBox
    Friend WithEvents Btn_Simpan As System.Windows.Forms.Button
    Friend WithEvents Cmb_jk As System.Windows.Forms.ComboBox
    Friend WithEvents txt_alamat As System.Windows.Forms.TextBox
    Friend WithEvents Btn_Cari As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Btn_Keluar As System.Windows.Forms.Button
    Friend WithEvents Btn_Hapus As System.Windows.Forms.Button
    Friend WithEvents Btn_Ubah As System.Windows.Forms.Button
    Friend WithEvents Btn_Tambah As System.Windows.Forms.Button
    Friend WithEvents Btn_bersih As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
End Class
