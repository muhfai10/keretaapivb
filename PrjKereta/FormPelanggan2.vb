﻿
Public Class FormPelanggan2
    Private conn As New MySqlConnection

    Private Sub Btn_Ubah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Ubah.Click
        Dim connString = "Database=kereta_api; Data source=localhost; user id=root; password=;"
        conn = New MySqlConnection(connString)
        conn.Open()

        Dim cmdUpdate As MySqlCommand = conn.CreateCommand

        If Txt_id.Text <> "" Then
            Try
                Dim sqlString As String = "update t_pelanggan set nama=@nama, alamat=@alamat, tgl_lahir=@tgl_lahir, jenis_kelamin=@jenis_kelamin, no_telp=@no_telp, email=@email where id_pelanggan=@id_pelanggan;"
                cmdUpdate.CommandText = sqlString
                cmdUpdate.Connection = conn
                cmdUpdate.Parameters.Add("@id_pelanggan", MySqlDbType.String, 20).Value = Txt_id.Text
                cmdUpdate.Parameters.Add("@nama", MySqlDbType.String, 25).Value = txt_nama.Text
                cmdUpdate.Parameters.Add("@alamat", MySqlDbType.String, 25).Value = txt_alamat.Text
                cmdUpdate.Parameters.Add("@tgl_lahir", MySqlDbType.Date).Value = DateTimePicker1.Text
                cmdUpdate.Parameters.Add("@jenis_kelamin", MySqlDbType.String, 25).Value = cmb_jk.Text
                cmdUpdate.Parameters.Add("@no_telp", MySqlDbType.String, 25).Value = txt_telp.Text
                cmdUpdate.Parameters.Add("@email", MySqlDbType.String, 25).Value = txt_email.Text
                cmdUpdate.ExecuteNonQuery()
                MessageBox.Show("data berhasil diubah", "My Project", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Bersihkan()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Terjadi kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Finally
                cmdUpdate.Dispose()
                conn.Close()
                conn = Nothing
            End Try
        End If
        TampilkanDaftar()

    End Sub

    Private Sub Btn_Hapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Hapus.Click
        Dim connString = "Database=kereta_api; Data source=localhost; user id=root; password=;"
        conn = New MySqlConnection(connString)
        conn.Open()

        Dim cmdDelete As MySqlCommand = conn.CreateCommand

        If Txt_id.Text <> "" Then
            If MessageBox.Show("Anda yakin data Pelanggan dengan Id_Pelanggan " & Txt_id.Text & " akan dihapus ?", "Peringatan!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                Try
                    Dim sqlString As String = "delete from t_pelanggan where id_pelanggan=@id_pelanggan;"
                    cmdDelete.CommandText = sqlString
                    cmdDelete.Connection = conn
                    cmdDelete.Parameters.Add("@id_pelanggan", MySqlDbType.String, 10).Value = Txt_id.Text
                    cmdDelete.ExecuteNonQuery()
                    MessageBox.Show("Data berhasil dihapus", "My Project", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Bersihkan()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Terjadi kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Finally
                    cmdDelete.Dispose()
                    'conn.Close()
                    conn = Nothing
                End Try
            Else
                MessageBox.Show("Data Tidak Jadi di Hapus")
            End If
            TampilkanDaftar()
        End If
    End Sub

    Private Sub Bersihkan()
        Txt_id.Text = ""
        txt_nama.Text = ""
        DateTimePicker1.Text = ""
        cmb_jk.Text = ""
        txt_telp.Text = ""
        txt_alamat.Text = ""
        Txt_id.Focus()
        Btn_Tambah.Enabled = True
        Btn_Ubah.Enabled = True
        Btn_Hapus.Enabled = True
    End Sub

    Private Sub Btn_Cari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Cari.Click
        CariByNIM()
    End Sub

    Private Sub Btn_Bersih_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_bersih.Click
        Bersihkan()
    End Sub

    Private Sub CariByNIM()
        Dim connString = "Database=kereta_api; Data source=localhost; user id=root; password=;"
        conn = New MySqlConnection(connString)
        conn.Open()

        Dim dr As MySqlDataReader = Nothing
        Dim cmdSelect As MySqlCommand = conn.CreateCommand

        Try
            If Txt_id.Text <> "" Then
                Dim sqlString As String = "select * from t_pelanggan where id_pelanggan=@id_pelanggan;"
                cmdSelect.Parameters.Add("@id_pelanggan", MySqlDbType.String, 10).Value = Txt_id.Text
                cmdSelect.CommandText = sqlString
                dr = cmdSelect.ExecuteReader

                If dr.Read() Then
                    Txt_id.Text = dr("id_pelanggan").ToString
                    txt_nama.Text = dr("nama").ToString
                    txt_alamat.Text = dr("alamat").ToString
                    cmb_jk.Text = dr("jenis_kelamin").ToString
                    DateTimePicker1.Text = dr("tgl_lahir").ToString
                    txt_telp.Text = dr("no_telp").ToString
                    txt_email.Text = dr("email").ToString
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Terjadi kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            cmdSelect.Dispose()
            conn.Close()
            conn = Nothing
        End Try
    End Sub

    Private Sub TampilkanDaftar()
        Dim da As New MySqlDataAdapter()
        Dim ds As New DataSet()

        Dim connString = "Database=kereta_api; Data source=localhost; user id=root; password=;"
        conn = New MySqlConnection(connString)


        Dim cmdSelect As MySqlCommand = conn.CreateCommand()
        Dim sqlString As String
        Try
            If Txt_id.Text <> "" Then
                sqlString = "select * from t_pelanggan where id_pelanggan=@id_pelanggan;"
                cmdSelect.Parameters.Add("@kode", MySqlDbType.String, 10).Value = Txt_id.Text
            Else
                sqlString = "select * from t_pelanggan;"
            End If

            cmdSelect.CommandText = sqlString

            da.SelectCommand = cmdSelect
            da.Fill(ds, "t_pelanggan")
            DataGridView1.DataSource = ds
            DataGridView1.DataMember = "t_pelanggan"
            DataGridView1.ReadOnly = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Terjadi kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            cmdSelect.Dispose()
            conn.Close()
            conn = Nothing
        End Try
    End Sub

    Private Sub Btn_Tambah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Tambah.Click
        Dim connString = "Database=kereta_api; Data source=localhost; user id=root; password=;"
        conn = New MySqlConnection(connString)
        conn.Open()

        Dim cmdInsert As MySqlCommand = conn.CreateCommand

        If Txt_id.Text <> "" Then
            Try
                Dim sqlString As String = "insert into t_pelanggan (id_pelanggan, nama, alamat, tgl_lahir, jenis_kelamin, no_telp, email) values (@id_pelanggan, @nama, @alamat, @tgl_lahir, @jenis_kelamin, @no_telp, @email);"
                cmdInsert.CommandText = sqlString
                cmdInsert.Connection = conn
                cmdInsert.Parameters.Add("@id_pelanggan", MySqlDbType.String, 5).Value = Txt_id.Text
                cmdInsert.Parameters.Add("@nama", MySqlDbType.String, 25).Value = txt_nama.Text
                cmdInsert.Parameters.Add("@alamat", MySqlDbType.String, 25).Value = txt_alamat.Text
                cmdInsert.Parameters.Add("@tgl_lahir", MySqlDbType.Date).Value = DateTimePicker1.Text
                cmdInsert.Parameters.Add("@jenis_kelamin", MySqlDbType.String, 25).Value = cmb_jk.Text
                cmdInsert.Parameters.Add("@no_telp", MySqlDbType.String, 25).Value = txt_telp.Text
                cmdInsert.Parameters.Add("@email", MySqlDbType.String, 25).Value = txt_email.Text
                cmdInsert.ExecuteNonQuery()
                MessageBox.Show("Data berhasil ditambahkan", "My Project", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Bersihkan()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Terjadi kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Finally
                cmdInsert.Dispose()
                conn.Close()
                conn = Nothing
            End Try
        End If
        TampilkanDaftar()
    End Sub

    Private Sub Btn_keluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_keluar.Click
        Me.Close()
    End Sub


    Private Sub FormPelanggan2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        TampilkanDaftar()
    End Sub

    Private Sub Btn_Cetak_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Cetak.Click
        Dim connString = "Database=kereta_api; Data source=localhost; user id=root; password=;"
        Dim da As New MySqlDataAdapter
        Dim ds As New DataSet
        Dim sqlString As String
        conn = New MySqlConnection(connString)
        Dim cmd As MySqlCommand = conn.CreateCommand
        conn.Open()

        Try
            ds.Clear()
            sqlString = "select * from t_pelanggan;"

            cmd = New MySqlCommand(sqlString, conn)
            da.SelectCommand = cmd
            da.Fill(ds, "t_pelanggan")

            Dim laporan As New rptDataPelanggan
            laporan.SetDataSource(ds)
            frmViewLaporan.CrystalReportViewer1.ReportSource = laporan
            frmViewLaporan.ShowDialog()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Terjadi Kegagalan !", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            conn.Close()
            conn = Nothing
        End Try
    End Sub
End Class