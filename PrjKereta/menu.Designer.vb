﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_menu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_menu))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btn_pelanggan = New System.Windows.Forms.Button()
        Me.btn_petugas = New System.Windows.Forms.Button()
        Me.btn_jadwal = New System.Windows.Forms.Button()
        Me.btn_tiket = New System.Windows.Forms.Button()
        Me.btn_gerbong = New System.Windows.Forms.Button()
        Me.btn_kelas = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial Narrow", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label1.Location = New System.Drawing.Point(276, 79)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(316, 25)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "SISTEM INFORMASI PT. KERETA API" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'btn_pelanggan
        '
        Me.btn_pelanggan.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_pelanggan.Location = New System.Drawing.Point(46, 24)
        Me.btn_pelanggan.Name = "btn_pelanggan"
        Me.btn_pelanggan.Size = New System.Drawing.Size(322, 110)
        Me.btn_pelanggan.TabIndex = 1
        Me.btn_pelanggan.Text = "PELANGGAN"
        Me.btn_pelanggan.UseVisualStyleBackColor = True
        '
        'btn_petugas
        '
        Me.btn_petugas.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_petugas.Location = New System.Drawing.Point(29, 32)
        Me.btn_petugas.Name = "btn_petugas"
        Me.btn_petugas.Size = New System.Drawing.Size(262, 92)
        Me.btn_petugas.TabIndex = 2
        Me.btn_petugas.Text = "PETUGAS"
        Me.btn_petugas.UseVisualStyleBackColor = True
        '
        'btn_jadwal
        '
        Me.btn_jadwal.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_jadwal.Location = New System.Drawing.Point(100, 315)
        Me.btn_jadwal.Name = "btn_jadwal"
        Me.btn_jadwal.Size = New System.Drawing.Size(199, 60)
        Me.btn_jadwal.TabIndex = 3
        Me.btn_jadwal.Text = "JADWAL "
        Me.btn_jadwal.UseVisualStyleBackColor = True
        '
        'btn_tiket
        '
        Me.btn_tiket.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tiket.Location = New System.Drawing.Point(100, 154)
        Me.btn_tiket.Name = "btn_tiket"
        Me.btn_tiket.Size = New System.Drawing.Size(199, 60)
        Me.btn_tiket.TabIndex = 4
        Me.btn_tiket.Text = "TIKET"
        Me.btn_tiket.UseVisualStyleBackColor = True
        '
        'btn_gerbong
        '
        Me.btn_gerbong.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_gerbong.Location = New System.Drawing.Point(100, 238)
        Me.btn_gerbong.Name = "btn_gerbong"
        Me.btn_gerbong.Size = New System.Drawing.Size(199, 60)
        Me.btn_gerbong.TabIndex = 5
        Me.btn_gerbong.Text = "GERBONG"
        Me.btn_gerbong.UseVisualStyleBackColor = True
        '
        'btn_kelas
        '
        Me.btn_kelas.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_kelas.Location = New System.Drawing.Point(100, 405)
        Me.btn_kelas.Name = "btn_kelas"
        Me.btn_kelas.Size = New System.Drawing.Size(199, 60)
        Me.btn_kelas.TabIndex = 6
        Me.btn_kelas.Text = "KELAS"
        Me.btn_kelas.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Gold
        Me.GroupBox1.Controls.Add(Me.btn_kelas)
        Me.GroupBox1.Controls.Add(Me.btn_pelanggan)
        Me.GroupBox1.Controls.Add(Me.btn_tiket)
        Me.GroupBox1.Controls.Add(Me.btn_gerbong)
        Me.GroupBox1.Controls.Add(Me.btn_jadwal)
        Me.GroupBox1.Location = New System.Drawing.Point(47, 127)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(427, 500)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "DATA PELANGGAN"
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Gold
        Me.GroupBox2.Controls.Add(Me.btn_petugas)
        Me.GroupBox2.Location = New System.Drawing.Point(522, 128)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(316, 154)
        Me.GroupBox2.TabIndex = 8
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "DATA PETUGAS"
        '
        'form_menu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(1157, 682)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label1)
        Me.DoubleBuffered = True
        Me.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Name = "form_menu"
        Me.Text = "menu"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btn_pelanggan As System.Windows.Forms.Button
    Friend WithEvents btn_petugas As System.Windows.Forms.Button
    Friend WithEvents btn_jadwal As System.Windows.Forms.Button
    Friend WithEvents btn_tiket As System.Windows.Forms.Button
    Friend WithEvents btn_gerbong As System.Windows.Forms.Button
    Friend WithEvents btn_kelas As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
End Class
