﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormPelanggan2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormPelanggan2))
        Me.Btn_Cetak = New System.Windows.Forms.Button()
        Me.Btn_Cari = New System.Windows.Forms.Button()
        Me.Btn_bersih = New System.Windows.Forms.Button()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.txt_alamat = New System.Windows.Forms.TextBox()
        Me.Btn_keluar = New System.Windows.Forms.Button()
        Me.Btn_Ubah = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Btn_Hapus = New System.Windows.Forms.Button()
        Me.Btn_Tambah = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.txt_email = New System.Windows.Forms.TextBox()
        Me.txt_telp = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_nama = New System.Windows.Forms.TextBox()
        Me.Txt_id = New System.Windows.Forms.TextBox()
        Me.LABEL5 = New System.Windows.Forms.Label()
        Me.LABEL6 = New System.Windows.Forms.Label()
        Me.LABEL_7 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmb_jk = New System.Windows.Forms.ComboBox()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Btn_Cetak
        '
        Me.Btn_Cetak.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Cetak.Image = CType(resources.GetObject("Btn_Cetak.Image"), System.Drawing.Image)
        Me.Btn_Cetak.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_Cetak.Location = New System.Drawing.Point(132, 33)
        Me.Btn_Cetak.Name = "Btn_Cetak"
        Me.Btn_Cetak.Size = New System.Drawing.Size(108, 40)
        Me.Btn_Cetak.TabIndex = 58
        Me.Btn_Cetak.Text = "CETAK"
        Me.Btn_Cetak.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Cetak.UseVisualStyleBackColor = True
        '
        'Btn_Cari
        '
        Me.Btn_Cari.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Cari.Image = CType(resources.GetObject("Btn_Cari.Image"), System.Drawing.Image)
        Me.Btn_Cari.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_Cari.Location = New System.Drawing.Point(376, 119)
        Me.Btn_Cari.Name = "Btn_Cari"
        Me.Btn_Cari.Size = New System.Drawing.Size(89, 30)
        Me.Btn_Cari.TabIndex = 57
        Me.Btn_Cari.Text = "CARI"
        Me.Btn_Cari.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Cari.UseVisualStyleBackColor = True
        '
        'Btn_bersih
        '
        Me.Btn_bersih.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_bersih.Image = CType(resources.GetObject("Btn_bersih.Image"), System.Drawing.Image)
        Me.Btn_bersih.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_bersih.Location = New System.Drawing.Point(15, 124)
        Me.Btn_bersih.Name = "Btn_bersih"
        Me.Btn_bersih.Size = New System.Drawing.Size(102, 35)
        Me.Btn_bersih.TabIndex = 55
        Me.Btn_bersih.Text = "BERSIH"
        Me.Btn_bersih.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_bersih.UseVisualStyleBackColor = True
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(204, 171)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(154, 26)
        Me.DateTimePicker1.TabIndex = 54
        '
        'txt_alamat
        '
        Me.txt_alamat.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_alamat.Location = New System.Drawing.Point(203, 285)
        Me.txt_alamat.Multiline = True
        Me.txt_alamat.Name = "txt_alamat"
        Me.txt_alamat.Size = New System.Drawing.Size(154, 91)
        Me.txt_alamat.TabIndex = 53
        '
        'Btn_keluar
        '
        Me.Btn_keluar.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_keluar.Image = CType(resources.GetObject("Btn_keluar.Image"), System.Drawing.Image)
        Me.Btn_keluar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_keluar.Location = New System.Drawing.Point(132, 124)
        Me.Btn_keluar.Name = "Btn_keluar"
        Me.Btn_keluar.Size = New System.Drawing.Size(108, 33)
        Me.Btn_keluar.TabIndex = 51
        Me.Btn_keluar.Text = "KELUAR"
        Me.Btn_keluar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_keluar.UseVisualStyleBackColor = True
        '
        'Btn_Ubah
        '
        Me.Btn_Ubah.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Ubah.Image = CType(resources.GetObject("Btn_Ubah.Image"), System.Drawing.Image)
        Me.Btn_Ubah.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_Ubah.Location = New System.Drawing.Point(15, 82)
        Me.Btn_Ubah.Name = "Btn_Ubah"
        Me.Btn_Ubah.Size = New System.Drawing.Size(102, 36)
        Me.Btn_Ubah.TabIndex = 49
        Me.Btn_Ubah.Text = "UBAH"
        Me.Btn_Ubah.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Ubah.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(71, 287)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(63, 20)
        Me.Label4.TabIndex = 52
        Me.Label4.Text = "ALAMAT"
        '
        'Btn_Hapus
        '
        Me.Btn_Hapus.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Hapus.Image = CType(resources.GetObject("Btn_Hapus.Image"), System.Drawing.Image)
        Me.Btn_Hapus.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_Hapus.Location = New System.Drawing.Point(132, 79)
        Me.Btn_Hapus.Name = "Btn_Hapus"
        Me.Btn_Hapus.Size = New System.Drawing.Size(108, 36)
        Me.Btn_Hapus.TabIndex = 50
        Me.Btn_Hapus.Text = "HAPUS"
        Me.Btn_Hapus.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Hapus.UseVisualStyleBackColor = True
        '
        'Btn_Tambah
        '
        Me.Btn_Tambah.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Tambah.Image = CType(resources.GetObject("Btn_Tambah.Image"), System.Drawing.Image)
        Me.Btn_Tambah.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.Btn_Tambah.Location = New System.Drawing.Point(15, 32)
        Me.Btn_Tambah.Name = "Btn_Tambah"
        Me.Btn_Tambah.Size = New System.Drawing.Size(102, 41)
        Me.Btn_Tambah.TabIndex = 48
        Me.Btn_Tambah.Text = "TAMBAH"
        Me.Btn_Tambah.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_Tambah.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(38, 397)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(755, 148)
        Me.DataGridView1.TabIndex = 47
        '
        'txt_email
        '
        Me.txt_email.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_email.Location = New System.Drawing.Point(203, 259)
        Me.txt_email.Name = "txt_email"
        Me.txt_email.Size = New System.Drawing.Size(154, 26)
        Me.txt_email.TabIndex = 46
        '
        'txt_telp
        '
        Me.txt_telp.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_telp.Location = New System.Drawing.Point(203, 229)
        Me.txt_telp.Name = "txt_telp"
        Me.txt_telp.Size = New System.Drawing.Size(154, 26)
        Me.txt_telp.TabIndex = 44
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(71, 258)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 20)
        Me.Label3.TabIndex = 43
        Me.Label3.Text = "E-MAIL"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(70, 231)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(89, 20)
        Me.Label2.TabIndex = 42
        Me.Label2.Text = "NO. TELPON"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(70, 205)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(113, 20)
        Me.Label1.TabIndex = 41
        Me.Label1.Text = "JENIS_KELAMIN"
        '
        'txt_nama
        '
        Me.txt_nama.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_nama.Location = New System.Drawing.Point(204, 144)
        Me.txt_nama.Name = "txt_nama"
        Me.txt_nama.Size = New System.Drawing.Size(154, 26)
        Me.txt_nama.TabIndex = 40
        '
        'Txt_id
        '
        Me.Txt_id.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_id.Location = New System.Drawing.Point(204, 117)
        Me.Txt_id.Name = "Txt_id"
        Me.Txt_id.Size = New System.Drawing.Size(154, 26)
        Me.Txt_id.TabIndex = 39
        '
        'LABEL5
        '
        Me.LABEL5.AutoSize = True
        Me.LABEL5.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LABEL5.Location = New System.Drawing.Point(71, 177)
        Me.LABEL5.Name = "LABEL5"
        Me.LABEL5.Size = New System.Drawing.Size(81, 20)
        Me.LABEL5.TabIndex = 38
        Me.LABEL5.Text = "TGL_LAHIR"
        '
        'LABEL6
        '
        Me.LABEL6.AutoSize = True
        Me.LABEL6.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LABEL6.Location = New System.Drawing.Point(71, 150)
        Me.LABEL6.Name = "LABEL6"
        Me.LABEL6.Size = New System.Drawing.Size(47, 20)
        Me.LABEL6.TabIndex = 37
        Me.LABEL6.Text = "NAMA"
        '
        'LABEL_7
        '
        Me.LABEL_7.AutoSize = True
        Me.LABEL_7.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LABEL_7.Location = New System.Drawing.Point(70, 119)
        Me.LABEL_7.Name = "LABEL_7"
        Me.LABEL_7.Size = New System.Drawing.Size(111, 20)
        Me.LABEL_7.TabIndex = 36
        Me.LABEL_7.Text = "ID_PELANGGAN"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.SystemColors.HighlightText
        Me.Label7.Font = New System.Drawing.Font("Arial Narrow", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(236, 41)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(307, 43)
        Me.Label7.TabIndex = 120
        Me.Label7.Text = "DATA PELANGGAN"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Btn_Tambah)
        Me.GroupBox1.Controls.Add(Me.Btn_Hapus)
        Me.GroupBox1.Controls.Add(Me.Btn_Cetak)
        Me.GroupBox1.Controls.Add(Me.Btn_Ubah)
        Me.GroupBox1.Controls.Add(Me.Btn_keluar)
        Me.GroupBox1.Controls.Add(Me.Btn_bersih)
        Me.GroupBox1.Location = New System.Drawing.Point(490, 117)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(256, 188)
        Me.GroupBox1.TabIndex = 121
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "OPERASI"
        '
        'cmb_jk
        '
        Me.cmb_jk.FormattingEnabled = True
        Me.cmb_jk.Items.AddRange(New Object() {"Laki-Laki", "Perempuan"})
        Me.cmb_jk.Location = New System.Drawing.Point(203, 204)
        Me.cmb_jk.Name = "cmb_jk"
        Me.cmb_jk.Size = New System.Drawing.Size(155, 21)
        Me.cmb_jk.TabIndex = 122
        '
        'FormPelanggan2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(832, 557)
        Me.Controls.Add(Me.cmb_jk)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Btn_Cari)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.txt_alamat)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.txt_email)
        Me.Controls.Add(Me.txt_telp)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txt_nama)
        Me.Controls.Add(Me.Txt_id)
        Me.Controls.Add(Me.LABEL5)
        Me.Controls.Add(Me.LABEL6)
        Me.Controls.Add(Me.LABEL_7)
        Me.Name = "FormPelanggan2"
        Me.Text = "FormPelanggan2"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Btn_Cetak As System.Windows.Forms.Button
    Friend WithEvents Btn_Cari As System.Windows.Forms.Button
    Friend WithEvents Btn_bersih As System.Windows.Forms.Button
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents txt_alamat As System.Windows.Forms.TextBox
    Friend WithEvents Btn_keluar As System.Windows.Forms.Button
    Friend WithEvents Btn_Ubah As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Btn_Hapus As System.Windows.Forms.Button
    Friend WithEvents Btn_Tambah As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents txt_email As System.Windows.Forms.TextBox
    Friend WithEvents txt_telp As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txt_nama As System.Windows.Forms.TextBox
    Friend WithEvents Txt_id As System.Windows.Forms.TextBox
    Friend WithEvents LABEL5 As System.Windows.Forms.Label
    Friend WithEvents LABEL6 As System.Windows.Forms.Label
    Friend WithEvents LABEL_7 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmb_jk As System.Windows.Forms.ComboBox
End Class
