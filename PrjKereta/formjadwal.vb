﻿Public Class formjadwal
    Private conn As New MySqlConnection
    Dim connString = "Database=kereta_api;data source=localhost;user id=root;password=;"
   
    Private Sub formjadwal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        tampilkandaftar()
    End Sub
    Sub tampilkandaftar()
        Dim da As New MySqlDataAdapter()
        Dim ds As New DataSet()
        Dim connString = "Database=kereta_api; Data source=localhost; user id=root; password=;"
        conn = New MySqlConnection(connString)
        Dim cmdSelect As MySqlCommand = conn.CreateCommand()
        Dim sqlString As String
        Try
            If txt_id.Text <> "" Then
                sqlString = "select * from t_jadwal where id_jadwal=@id_jadwal;"
                cmdSelect.Parameters.Add("@id_jadwal", MySqlDbType.String, 10).Value = txt_id.Text
            Else
                sqlString = "select * from t_jadwal;"
            End If
            cmdSelect.CommandText = sqlString
            da.SelectCommand = cmdSelect
            da.Fill(ds, "t_jadwal")
            DataGridView1.DataSource = ds
            DataGridView1.DataMember = "t_jadwal"
            DataGridView1.ReadOnly = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Terjadi kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            cmdSelect.Dispose()
            conn.Close()
            conn = Nothing
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Sub bersihkan()
        txt_id.Text = ""
        DateTimePicker1.Text = ""
        Btn_tambah.Enabled = True
    End Sub

    Private Sub Btn_Cari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Cari.Click
        conn = New MySqlConnection(connString)
        conn.Open()

        Dim dr As MySqlDataReader = Nothing
        Dim cmdSelect As MySqlCommand = conn.CreateCommand()
        Try
            If txt_id.Text <> "" Then
                Dim sqlString As String = "Select * from t_jadwal where id_jadwal=@id_jadwal;"
                cmdSelect.Parameters.Add("@id_jadwal", MySqlDbType.String, 20).Value = txt_id.Text
                cmdSelect.CommandText = sqlString
                dr = cmdSelect.ExecuteReader

                If dr.Read() Then
                    txt_id.Text = dr("id_jadwal").ToString
                    DateTimePicker1.Text = dr("waktu_keberangkatan").ToString
                    Btn_tambah.Enabled = False
                End If
                tampilkandaftar()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Terjadi Kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            cmdSelect.Dispose()
            'conn.Close()
            conn = Nothing
        End Try
    End Sub

    Private Sub Btn_Keluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Keluar.Click
        Me.Close()
    End Sub

    Private Sub Btn_tambah_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_tambah.Click
        conn = New MySqlConnection(connString)
        conn.Open()
        Dim cmdInsert As MySqlCommand = conn.CreateCommand
        If txt_id.Text <> "" Then
            Try
                Dim SqlString As String = " insert into t_jadwal (id_jadwal,waktu_keberangkatan) values (@id_jadwal,@waktu_keberangkatan);"
                cmdInsert.CommandText = SqlString
                cmdInsert.Connection = conn
                cmdInsert.Parameters.Add("@id_jadwal", MySqlDbType.String, 10).Value = txt_id.Text()
                cmdInsert.Parameters.Add("@waktu_keberangkatan", MySqlDbType.Timestamp).Value = DateTimePicker1.Text
                cmdInsert.ExecuteNonQuery()
                MessageBox.Show("data Berhasil ditambahkan", "My Project", MessageBoxButtons.OK, MessageBoxIcon.Information)
                bersihkan()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Terjadi Kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Finally
                cmdInsert.Dispose()
                'conn.Close()
                conn = Nothing
            End Try
        End If
        tampilkandaftar()
    End Sub

    Private Sub Btn_Hapus_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Hapus.Click
        conn = New MySqlConnection(connString)
        conn.Open()

        Dim cmdDelete As MySqlCommand = conn.CreateCommand
        If txt_id.Text <> "" Then
            If MessageBox.Show("Anda yakin data jadwal dengan id jadwal " & txt_id.Text & " akan dihapus?", "Peringatan!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                Try
                    Dim sqlString As String = "delete from t_jadwal where id_jadwal=@id_jadwal;"
                    cmdDelete.CommandText = sqlString
                    cmdDelete.Connection = conn
                    cmdDelete.Parameters.Add("@id_jadwal", MySqlDbType.String, 20).Value = txt_id.Text
                    cmdDelete.ExecuteNonQuery()
                    MessageBox.Show("data Berhasil dihapus", "My Project", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    bersihkan()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Terjadi Kegagalan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Finally
                    cmdDelete.Dispose()
                    'conn.Close()
                    conn = Nothing
                End Try
            End If
        End If
        tampilkandaftar()
    End Sub

    Private Sub Btn_Bersih_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Bersih.Click
        bersihkan()
    End Sub
End Class